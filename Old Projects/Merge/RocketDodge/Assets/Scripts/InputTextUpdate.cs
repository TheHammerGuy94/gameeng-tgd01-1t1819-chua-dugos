﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InputState {
    start, wrong, correct
}

public static class InputStateMethods {
    public static string getString(this InputState x) {
        switch (x) {
            case InputState.start: return "type for movement";
            case InputState.wrong: return "INPUT ERROR";
            case InputState.correct: return "CORRECT! Executing...";
            default: return "";
        }
    }
}

public class InputTextUpdate : MonoBehaviour {
    public KeyBoardInput key;
    public InputState state;
    private Text x;

    private string prevText = "";

	// Use this for initialization
	void Start () {
        if (key == null) {
            key = GameController.instance.f.gameObject.GetComponent<KeyBoardInput>();
        }
        x = GetComponent<Text>();
        state = InputState.start;
        RefeshText();
	}
	
	// Update is called once per frame
	void Update () {
        if (prevText != key.input) {
            prevText = key.input;
            RefeshText();
        }
	}

    public void setState(InputState s) {
        this.state = s;
    }

    private void RefeshText() {

        if (prevText.Length == 0)
        {
            x.text = state.getString();//bug found
        }
        else
        {
            x.text = key.input;
        }
    }
}
