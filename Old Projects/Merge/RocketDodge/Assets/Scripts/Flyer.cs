﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flyer : MonoBehaviour {
    public static float hitTime = 1.0f; //think invincibility frames
    public Rigidbody2D rb;
    public float speed = 15.0f;
    public float hitTimeRemain = 0.0f;
    public int startSection = 0, targSection = 0,score = 0;
    public float gas = 1.0f, gasRate = 1.0f / 10.0f;
    public bool hit = false;
    public Animator anim;

	void Start () {
        rb = GetComponent<Rigidbody2D>();

        if (rb.position.y != LaneToY(targSection)) {
            startSection = (int)Mathf.Sign(rb.position.y - LaneToY(targSection));
        }
        if (anim == null) {
            anim = gameObject.GetComponent<Animator>();
        }
    }
	
	void Update () {

        UpdateHit();
        UpdateGas();
        /*
        if (Input.anyKeyDown) { //This is to be modified with the keyboard inputs
            if (Input.GetKey(KeyCode.UpArrow)) MoveUp();
            if (Input.GetKey(KeyCode.DownArrow)) MoveDown();
        }*/
        Move();
        MoveStop();
	}

    public void MoveUp()
    {
        if (startSection == targSection) {
            targSection++;
            if (targSection > GameController.instance.getMaxLane())
                targSection = GameController.instance.getMaxLane();
        }
    }
    public void MoveDown()
    {
        if (startSection == targSection) {
            targSection--;
            if (targSection < GameController.instance.getMinLane())
                targSection = GameController.instance.getMinLane();
        }
    }

    private void Move() {
        if (startSection != targSection) {
            Move(targSection - startSection > 0);
        }
    }

    private void Move(bool goingUp) {
        int dir = ((goingUp) ? 1 : -1);
        rb.velocity = Vector2.up*speed *dir;
        if (goingUp) anim.SetTrigger("upT");
        else anim.SetTrigger("dnT");

    }

    private void MoveStop() {
        if (ProgressCalc(rb.position.y, LaneToY(startSection), LaneToY(targSection)) >= 1.0f){
            rb.velocity = Vector2.zero;
            rb.position.Set(rb.position.x, LaneToY(targSection));
            startSection = targSection;
            anim.SetTrigger("idleT");
        }
    }

    public void FlyerHit() {
        if (!hit) {
            hit = true;
            hitTimeRemain = hitTime;
            gas -= 0.1f;
            //code for game over dosent go here
        }
    }
    public void GameOverAnim() {
        anim.SetTrigger("gameOverAnime");
    }
    public void FlyerRefill(float x) {
        gas += x;
        if (gas > 1.0f) gas = 1.0f;
    }
    public void AddScore() {
        score++;
    }

    public void AddScore(int x) {
        this.score += x;
    }

    public void UpdateHit() {
        if (hit)
        {
            hitTimeRemain -= Time.deltaTime;
            if (hitTimeRemain <= 0.0f)
            {
                hitTimeRemain = 0.0f;
                hit = false;
            }
        }
    }
    public void UpdateGas() {
        gas -= gasRate * Time.deltaTime;
        if (gas < 0.0f) gas = 0.0f;
    }

    public static float ProgressCalc(float input, float start, float end) {
        return (input - start) / (end - start);
    }

    public static float LaneToY(int x) {
        return -0.3f +x * 1.35f;
    }
}
