﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBoardInputUI : MonoBehaviour {
    public KeyBoardInput key;
    public Text upDisplay, downDisplay;


    // Use this for initialization
    void Start () {
        if (key == null) {
            key = GameController.instance.f.gameObject.GetComponent<KeyBoardInput>();
        }
        if (upDisplay == null) {
            upDisplay = GameObject.Find("upTextCurrent").GetComponent<Text>();
        }
        if (downDisplay == null) {
            downDisplay = GameObject.Find("downTextCurrent").GetComponent<Text>();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (upDisplay.text != key.goUp) {
            upDisplay.text = key.goUp;
        }
        if (downDisplay.text != key.goDn) {
            downDisplay.text = key.goDn;
        }

	}
}
