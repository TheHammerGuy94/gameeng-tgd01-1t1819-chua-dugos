﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class KeyBoardInput : MonoBehaviour {
    public Flyer x;
    private WeightedRand<string> upList, downList;
    public InputTextUpdate inputUpdate;
    public TextAsset wordSrc;
    public string input;
    public bool gameJ = true; // game journalist mode
    public string goUp, goDn;
    
	void Start () {
        //making sure flyer exists
        if (x == null)
            x = gameObject.GetComponent<Flyer>();
        if (x == null)
            x = GameController.instance.f;

        input = "";
        LoadFile();
        reloadOps();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown) {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                GameController.instance.isPaused = !GameController.instance.isPaused;
            }
            if (Input.inputString.Length > 0 && !GameController.instance.isPaused) {
                char c = Input.inputString[0];
                c = char.ToLower(c);

                switch (c) {
                    case '0': resetWord();//if the player made a mistake, he can press the '0' key
                        break;
                    case '9': reloadOps();//reload current options
                        break;
                    default: CheckInput(c);
                        break;
                }
            }

        }

	}
    private void resetWord() {
        input = "";
    }

    private void reloadOps() {
        goUp = upList.GetItemRand();

        goDn = downList.GetItemRand();
    }

    private void CheckInput(char c) {
        input += c;
        if (!goUp.StartsWith(input) && !goDn.StartsWith(input)) {
            inputUpdate.setState(InputState.wrong);
            resetWord();
        }

        if (input.Length == goUp.Length || input.Length == goDn.Length)
        {
            if (input == goUp || input == goDn)
            {
                if (input == goUp)
                {
                    x.MoveUp();
                    goUp = upList.GetItemRand();
                }
                else if (input == goDn)
                {
                    x.MoveDown();
                    goDn = downList.GetItemRand();
                }
                x.AddScore(gameJ ? 1 : input.Length);
                inputUpdate.setState(InputState.correct);
                resetWord();
            }
        }
    }

    private void LoadFile() {
        List<string> dnList, upList;
        string dnProxList, upProxList;
        int longestWord = 0, shortestWord = 0;

        this.upList = new WeightedRand<string>();
        this.downList = new WeightedRand<string>();

        dnList = new List<string>();
        upList = new List<string>();

        if (wordSrc == null) {
            wordSrc = Resources.Load<TextAsset>("wordList.txt");
        }
        if (wordSrc == null)
        {
            Debug.Log("file cannot load");
        }
        else {
            Debug.Log("file loaded");
        }

        dnProxList = wordSrc.text.Split('\n')[0];
        upProxList = wordSrc.text.Split('\n')[1];

        dnList = dnProxList.Split(',').ToList<string>();
        upList = upProxList.Split(',').ToList<string>();

        for (int i = 0; i < dnList.Count; i++) dnList[i] = dnList[i].Trim();
        for (int i = 0; i < upList.Count; i++) upList[i] = upList[i].Trim();

        upList.RemoveAll(s => s.Length == 0);
        dnList.RemoveAll(s => s.Length == 0);


        if (dnList.Count < 1) dnList.Add("fall");
        if (upList.Count < 1) upList.Add("rise");

        longestWord = shortestWord = dnList[0].Length;
        foreach (string s in dnList) {
            longestWord = Mathf.Max(s.Length, longestWord);
            shortestWord = Mathf.Min(s.Length, shortestWord);
        }

        foreach (string s in dnList) {
            this.downList.AddItem(s, (gameJ ? longestWord - s.Length + shortestWord : s.Length));
        }

        longestWord = shortestWord = dnList[0].Length;
        foreach (string s in upList) {
            longestWord = Mathf.Max(s.Length, longestWord);
            shortestWord = Mathf.Min(s.Length, shortestWord);
        }
           

        foreach (string s in upList){
            this.upList.AddItem(s, (gameJ ? longestWord - s.Length + shortestWord : s.Length));
        }
    }
}
