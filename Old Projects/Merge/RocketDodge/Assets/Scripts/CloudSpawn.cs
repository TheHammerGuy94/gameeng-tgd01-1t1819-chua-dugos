﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawn : MonoBehaviour {
    public GameObject cloudPrefab;
    public float time = 0.0f;
    public float spawnDelay = 1f;

    private float rangeY = 5;
    private float speedMin = 2, speedMax = 5;
    private float scaleMin = 0.2f, scaleMax = 0.5f;
    private float minZ = 2, maxZ = 12;

    public void Spawn() {
        GameObject o = Instantiate(cloudPrefab);
        Rigidbody2D rb = o.GetComponent<Rigidbody2D>();
        float rand = Random.value;
        o.transform.position = new Vector3(gameObject.transform.position.x, Random.Range(-rangeY, rangeY), (maxZ - minZ) * rand + minZ);
        float scale = (scaleMax - scaleMin) * rand + scaleMin;
        o.transform.localScale.Set(scale,scale,scale);
        rand = 1 - rand;
        rb.velocity = Vector3.left * ((speedMax - speedMin) * rand + speedMin);

    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        time = time + Time.deltaTime;
        if (time > spawnDelay)
        {
            time = time % spawnDelay;
            Spawn();
        }
    }
}
