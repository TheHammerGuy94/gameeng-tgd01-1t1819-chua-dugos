﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsUI : MonoBehaviour
{
    public Text score,gameOver,spacePrompt;
    public GameObject health;
    private int scorePrev;
    private float time = 0f;
    public Flyer f;

    // Use this for initialization
    void Start()
    {
        scorePrev = -1;
        if (f == null)
            f = GameController.instance.f;
        if (score == null)
            score = GameObject.Find("scoreUI").GetComponent<Text>();
        if(gameOver == null)
            gameOver = GameObject.Find("gameOverText").GetComponent<Text>();
        if (spacePrompt == null)
            spacePrompt = GameObject.Find("spacePrompt").GetComponent<Text>();
        if (health == null)
            health = GameObject.Find("HPBar");
        if (f == null)
            f = GameController.instance.f;


        gameOver.enabled = spacePrompt.enabled = false;

    }

    // Update is called once per frame
    void Update(){

        if (!GameController.instance.gameOver)
        {
            if (f.score != this.scorePrev)
            {
                UpdateScore();
            }

            UpdateHealth();
        }
        else {
            showGameOver();
        }

    }

    private void UpdateScore() {
        string score = "Score:{0}";
        scorePrev = f.score;
        this.score.text = string.Format(score, scorePrev);

    }
    private void UpdateHealth() {
        health.transform.localScale = new Vector3(f.gas, 1, 1);
    }

    private void showGameOver() {
        if (!gameOver.enabled) {
            gameOver.enabled = true;
        }
        else
        {
            time += Time.unscaledDeltaTime;
            if (time >= 2.0f) {
                spacePrompt.enabled = true;
                if (Input.GetKeyDown(KeyCode.Space))
                    GameController.instance.RestartScene();
            }

        }

    }
}
