﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class WeightedRand<T>
{
    private List<T> list;
    private List<int> weight;
    private int totalWeight = 0;

    public WeightedRand() {
        list = new List<T>();
        weight = new List<int>();
    }

    public void AddItem(T item, int weight) {
        list.Add(item);
        this.weight.Add(weight);
        totalWeight += weight;
    }

    public List<T> GetList() {
        return this.list;
    }

    public int GetTotalWeight() {
        return totalWeight;
    }
    public int GetItemRandIndex() {
        int i, x = UnityEngine.Random.Range(0, totalWeight);
        for (i = 0; x > this.weight[i]; i++)
            x -= this.weight[i];
        return i;
    }
    public T GetItemRand() {
        return this.list[GetItemRandIndex()];
    }


}
