﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flyer : MonoBehaviour {
    public static float hitTime = 1.0f; //think invincibility frames
    public Rigidbody2D rb;
    public float speed = 15.0f;
    public float hitTimeRemain = 0.0f;
    public int startSection = 0, targSection = 0;
    public int score = 0, lives = 5;
    public bool hit = false;

	void Start () {
        rb = GetComponent<Rigidbody2D>();

        if (rb.position.y != LaneToY(targSection)) {
            startSection = (int)Mathf.Sign(rb.position.y - LaneToY(targSection));
        }

    }
	
	void Update () {

        UpdateHit();
        /*
        if (Input.anyKeyDown) { //This is to be modified with the keyboard inputs
            if (Input.GetKey(KeyCode.UpArrow)) MoveUp();
            if (Input.GetKey(KeyCode.DownArrow)) MoveDown();
        }*/
        Move();
        MoveStop();
	}
    


    public void MoveUp()
    {
        if (startSection == targSection) {
            targSection++;
            if (targSection > 1)
                targSection = 1;
        }
    }
    public void MoveDown()
    {
        if (startSection == targSection) {
            targSection--;
            if (targSection < -1)
                targSection = -1;
        }
    }

    private void Move() {
        if (startSection != targSection) {
            Move(targSection - startSection > 0);
        }
    }

    private void Move(bool goingUp) {
        int dir = ((goingUp) ? 1 : -1);
        rb.velocity = Vector2.up*speed *dir;
    }

    private void MoveStop() {
        if (ProgressCalc(rb.position.y, LaneToY(startSection), LaneToY(targSection)) >= 1.0f){
            rb.velocity = Vector2.zero;
            rb.position.Set(rb.position.x, LaneToY(targSection));
            startSection = targSection;
        }
    }

    public void FlyerHit() {
        if (!hit) {
            hit = true;
            hitTimeRemain = hitTime;
            lives--;
            //code for game over dosent go here
        }
    }
    public void AddScore() {
        score++;
    }

    public void AddScore(int x) {
        this.score += x;
    }

    public void UpdateHit() {
        if (hit)
        {
            hitTimeRemain -= Time.deltaTime;
            if (hitTimeRemain <= 0.0f)
            {
                hitTimeRemain = 0.0f;
                hit = false;
            }
        }
    }

    public static float ProgressCalc(float input, float start, float end) {
        return (input - start) / (end - start);
    }

    public static float LaneToY(int x) {
        return -0.3f +x * 2.7f;
    }
}
