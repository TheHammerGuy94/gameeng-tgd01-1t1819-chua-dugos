﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsSpawn : MonoBehaviour
{
    public GameObject obsPrefab;
    public float time = 0.0f;
    public float spawnDelay = 1.5f;

    void Spawn()
    {
        int x = Random.Range(-1, 2);
        GameObject o = Instantiate(obsPrefab);

        o.GetComponent<Obstacle>().lane = x;

        o.transform.position = new Vector2(10.0f, 0.0f);

        Destroy(o, 10f);
    }

	// Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        time = time + Time.deltaTime;

        if(time > spawnDelay)
        {
            time = time % spawnDelay;

            Spawn();
        }
	}
}
