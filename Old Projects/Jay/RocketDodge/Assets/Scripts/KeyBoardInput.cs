﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class KeyBoardInput : MonoBehaviour {
    public Flyer x;
    private WeightedRand<string> upList, downList;
    public TextAsset wordSrc;
    public string input;
    public bool gameJ = true; // game journalist mode
    public string goUp, goDn;
    public Text upDisplay, downDisplay, inputDisplay;
    
	void Start () {
        input = "";
        LoadFile();
        reloadOps();

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKeyDown) {
            char c = Input.inputString[0];

            switch (c) {
                case '0': resetWord();//if the player made a mistake, he can press the '0' key
                    break;
                case '9': reloadOps();//reload current options
                    break;
                default: CheckInput(c);
                    break;
            }
        }
        inputDisplay.text = input;
	}
    private void resetWord() {
        input = "";
    }

    private void reloadOps() {
        goUp = upList.GetItemRand();
        upDisplay.text = goUp;

        goDn = downList.GetItemRand();
        downDisplay.text = goDn;
    }

    private void CheckInput(char c) {
        input += c;
        if (!goUp.StartsWith(input) && !goDn.StartsWith(input))
            resetWord();

        if (input.Length == goUp.Length || input.Length == goDn.Length)
        {
            if (input == goUp || input == goDn)
            {
                if (input == goUp)
                {
                    x.MoveUp();
                    goUp = upList.GetItemRand();
                    upDisplay.text = goUp;
                }
                else if (input == goDn)
                {
                    x.MoveDown();
                    goDn = downList.GetItemRand();
                    downDisplay.text = goDn;
                }
                x.AddScore(gameJ ? 1 : input.Length);
                resetWord();
            }
        }
    }

    private void LoadFile() {
        List<string> dnList, upList;
        string dnProxList, upProxList;
        int longestWord = 0, shortestWord = 0;

        this.upList = new WeightedRand<string>();
        this.downList = new WeightedRand<string>();

        dnList = new List<string>();
        upList = new List<string>();

        if (wordSrc == null) {
            wordSrc = Resources.Load<TextAsset>("wordList.txt");
        }
        if (wordSrc == null)
        {
            Debug.Log("file cannot load");
        }
        else {
            Debug.Log("file loaded");
        }

        dnProxList = wordSrc.text.Split('\n')[0];
        upProxList = wordSrc.text.Split('\n')[1];

        dnList = dnProxList.Split(',').ToList<string>();
        upList = upProxList.Split(',').ToList<string>();


        if (dnList.Count < 1) dnList.Add("fall");
        if (upList.Count < 1) upList.Add("rise");

        longestWord = shortestWord = dnList[0].Length;
        foreach (string s in dnList) {
            longestWord = Mathf.Max(s.Length, longestWord);
            shortestWord = Mathf.Min(s.Length, shortestWord);
        }

        foreach (string s in dnList) {
            this.downList.AddItem(s, (gameJ ? longestWord - s.Length + shortestWord : s.Length));
        }

        longestWord = shortestWord = dnList[0].Length;
        foreach (string s in upList) {
            longestWord = Mathf.Max(s.Length, longestWord);
            shortestWord = Mathf.Min(s.Length, shortestWord);
        }
           

        foreach (string s in upList){
            this.upList.AddItem(s, (gameJ ? longestWord - s.Length + shortestWord : s.Length));
        }
    }
}
