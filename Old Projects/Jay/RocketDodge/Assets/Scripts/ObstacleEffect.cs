﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObstacleEffect : MonoBehaviour {
    public Collider2D bx;
	// Use this for initialization
	void Start () {
        bx = GetComponent<Collider2D>();
    }
	
	// Update is called once per frame
	void Update () {
    }

    private void OnTriggerEnter2D(Collider2D c){
        if (c.GetComponent<Flyer>() != null) {
            Debug.Log("Hit Detected");
            EffectTrigger(c.GetComponent<Flyer>());
        }
    }
    public abstract void EffectTrigger(Flyer f);

}
