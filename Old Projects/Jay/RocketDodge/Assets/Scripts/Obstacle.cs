﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    public Rigidbody2D rb;
    public float speed = 10;
    public int lane = 0;

	void Start () {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(-1.0f * speed, 0.0f);
	}

	void Update () {
        if (rb.velocity.magnitude != speed) {
            rb.velocity.Normalize();
            rb.velocity *= speed;
        }
        if (rb.gameObject.transform.position.y != Flyer.LaneToY(lane)) {
            rb.transform.position = new Vector2(rb.transform.position.x, Flyer.LaneToY(lane));
        }
	}

    public void SetLane(int x){
        this.lane = x;
    }
}
