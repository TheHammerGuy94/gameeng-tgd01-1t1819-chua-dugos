﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]

public class Tiling : MonoBehaviour
{
    public int offsetX = 2;
    public bool hasARightBuddy = false;
    public bool hasALeftBuddy = false;

    public bool shouldReverseScale = false;

    private float spriteWidth = 0.0f;
    private Camera cameraReference;

    private Transform thisTransform;

    void Awake()
    {
        cameraReference = Camera.main;
        thisTransform = transform;
    }


	// Use this for initialization
	void Start()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteWidth = spriteRenderer.bounds.size.x;
	}
	
	// Update is called once per frame
	void Update()
    {
		if (hasALeftBuddy == false || hasARightBuddy == false)
        {
            float cameraHorizontalExtend = cameraReference.orthographicSize * (Screen.width / Screen.height);

            float edgeVisiblePositionRight = (thisTransform.position.x + (spriteWidth / 2)) - cameraHorizontalExtend;
            float edgeVisiblePositionLeft = (thisTransform.position.x - (spriteWidth / 2)) + cameraHorizontalExtend;

            if (cameraReference.transform.position.x >= edgeVisiblePositionRight - offsetX && hasARightBuddy == false)
            {
                MakeNewBuddy(1);
                hasARightBuddy = true;
            }

            else if (cameraReference.transform.position.x <= edgeVisiblePositionLeft - offsetX && hasALeftBuddy == false)
            {
                MakeNewBuddy(-1);
                hasALeftBuddy = true;
            }

        }
    }

    void MakeNewBuddy(int leftorRight)
    {
        Vector3 newPosition = new Vector3(((thisTransform.position.x + spriteWidth) * leftorRight), thisTransform.position.y, thisTransform.position.z);
        Transform newBuddy = (Transform)Instantiate(thisTransform, newPosition, thisTransform.rotation);

        if (shouldReverseScale == true)
        {
            newBuddy.localScale = new Vector3(newBuddy.localScale.x * -1, newBuddy.localScale.y, newBuddy.localScale.z);
        }

        newBuddy.parent = thisTransform.parent;

        if (leftorRight > 0)
        {
            newBuddy.GetComponent<Tiling>().hasALeftBuddy = true;
        }

        else
        {
            newBuddy.GetComponent<Tiling>().hasARightBuddy = true;
        }
    }
}
