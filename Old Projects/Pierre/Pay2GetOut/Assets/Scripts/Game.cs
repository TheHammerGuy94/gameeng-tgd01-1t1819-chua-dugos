﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game instance = null;
    public Player p;
    public CoinSpawn cs;
    public Timer tmr;


    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    // Use this for initialization
    void Start()
    {
        if (p == null) 
            p = GameObject.Find("Player").GetComponent<Player>();
        if (tmr == null)
            tmr = GetComponent<Timer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StopSpawn(){
        cs.ActiveSpawn(false);
        
    }
    public void FinishLine(){
        tmr.play = false;
        SceneSettings.GetInstance().SetSetting("score", tmr.timer.ToString("f4"));
        HighScore(tmr.timer);
        //load gameoverscreen here
        Invoke("LoadGameOver", 0.25f);
    }
    public void LoadGameOver(){
        LoadGameOver(true);
    }

    public void LoadGameOver(bool valid) {
        if(!valid)
            SceneSettings.GetInstance().SetSetting("score", "INVALID!!!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void HighScore(float x){
        string sc = SceneSettings.GetInstance().GetSetting("HighScore");
        float hs = x;
        if(sc.Length >0)
        {
            hs = float.Parse(sc);
            if (hs > x)
                hs = x;
        }
        SceneSettings.GetInstance().SetSetting("HighScore", hs.ToString("f4"));


    }
}
