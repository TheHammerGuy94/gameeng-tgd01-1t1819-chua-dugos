﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
    public CoinSpawn spawner;
    public float weight;

	// Use this for initialization
	void Start () {
        if (!spawner)
            spawner = GameObject.Find("CoinSpawn").GetComponent<CoinSpawn>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetPrefab(float x, float y, float mass) {
        this.transform.position = new Vector2(x, y);
        this.weight= mass;
    }

    private void OnDestroy()
    {
        spawner.DestroyedCoin(this);
    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        Player p = c.GetComponent<Player>();
        if (p) {
            p.AddMass(this.weight);
            spawner.Pickup();
            spawner.SpawnExplosion(transform.position, (int)weight);
            Destroy(gameObject); // fix this...
        }
    }
}
