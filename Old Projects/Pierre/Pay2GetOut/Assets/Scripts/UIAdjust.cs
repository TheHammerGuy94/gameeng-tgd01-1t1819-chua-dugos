﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIAdjust : MonoBehaviour
{
    public static float xDefault = -9f, yDefault = -5f;
    public static float xMultDefault = 18, yMultDefault = 10;
    public GameObject objectFollow;
    private Text txt;

    public float xOffset, yOffset;

    // Use this for initialization
    void Start()
    {
        if (txt == null) GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 ui = Camera.main.WorldToViewportPoint(objectFollow.transform.position);
        ui.Set(ui.x * xMultDefault + xDefault + xOffset,
               ui.y * yMultDefault + yDefault + yOffset,
               ui.z
              );
        transform.position = ui;
    }
}
