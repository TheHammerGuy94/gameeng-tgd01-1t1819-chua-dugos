﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ValidArea : MonoBehaviour
{
    public Text where;
    public float maxTime = 5f;
    private float time;
    private bool timer = false;


    // Use this for initialization
    void Start()
    {
        SetVisibleUI(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(timer){
            time += Time.deltaTime;
            if (time >= maxTime)
            {
                Game.instance.LoadGameOver(false);
            }
        }

    }


    public void SetVisibleUI(bool x){
        where.gameObject.SetActive(x);
        timer = x;
    }


    private void OnTriggerEnter2D(Collider2D c)
    {
        Player p = c.gameObject.GetComponent<Player>();
        if (p)
        {
            SetVisibleUI(false);

        }
    }

    private void OnTriggerExit2D(Collider2D c)
    {
        Player p = c.gameObject.GetComponent<Player>();
        if(p){
            SetVisibleUI(true);
        }
    }
}
