﻿using UnityEngine;
using System.Collections;

public class BoundTimeStopper : Bounds
{
    public override void EnterAction(Player p)
    {
        if(!Game.instance.tmr.down &&p.GetMass() >10){
            Game.instance.FinishLine();
        }
    }

    public override void ExitAction(Player p){}
}
