﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimerUI : MonoBehaviour
{
    private Text txt;
    public Timer tmr;
    public Player p;

    // Use this for initialization
    void Start()
    {
        txt = GetComponent<Text>();
        if (tmr == null)
            tmr = Game.instance.gameObject.GetComponent<Timer>();
        if (p == null)
            p = Game.instance.p;
    }

    // Update is called once per frame
    void Update()
    {
        txt.text = tmr.timer.ToString("f2");
    }


}
