﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeightUI : GuiUpdate{
    public Player p;
    public float prevWeight = -1f;

    public override bool CheckPrevValue()
    {
        return prevWeight.CompareTo(p.GetMass()) != 0;
    }

    public override void ExtraStart()
    {
        if (p == null)
            p = Game.instance.p;
    }

    public override string GetValue()
    {
        return prevWeight.ToString("f2");
    }

    public override void UpdateValue()
    {
        prevWeight = p.GetMass();
    }

}
