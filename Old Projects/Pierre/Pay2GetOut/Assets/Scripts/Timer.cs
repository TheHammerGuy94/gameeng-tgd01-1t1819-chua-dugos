﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour{
    public Text git;
    public AudioClip beep;
    private AudioSource src;

    public float maxTime = 10,timer;
    private float beepRate = 1.0f;
    private float beepTimer = 0.0f;
    public bool down;
    public bool play;
    // Use this for initialization
    void Start()
    {
        if(git == null)
            git = GameObject.Find("GOHui").GetComponent<Text>();

        src = GetComponent<AudioSource>();
        src.clip = beep;

        down = true;
        play = true;
    }

    // Update is called once per frame
    void Update()
    {

        beepTimer += Time.deltaTime;
        if (beepTimer >= beepRate){
            BeepOnce();
            beepTimer %= beepRate;

        }

        if(down){
            if (timer > 0.0f)
                timer -= Time.deltaTime;
            else{
                timer = 0.0f;
                Notify();
            }
        }
        else if(play)
            timer += Time.deltaTime;
    }
    public void BeepOnce(){
        src.Play();
    }



    public void Notify(){
        //code for Notify goes here.
        Game.instance.StopSpawn();
        down = false;
        beepRate /= 2;
        git.gameObject.SetActive(true);

    }

}
