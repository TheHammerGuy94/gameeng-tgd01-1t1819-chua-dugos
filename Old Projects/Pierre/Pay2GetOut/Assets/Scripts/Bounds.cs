﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bounds : MonoBehaviour {
    private Collider2D cd;

	// Use this for initialization
	void Start () {
        cd = GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player p = collision.gameObject.GetComponent<Player>();
        if (p) {
            EnterAction(p);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Player p = collision.gameObject.GetComponent<Player>();
        if (p)
        {
            ExitAction(p);
        }
    }

    public abstract void ExitAction(Player p);
    public abstract void EnterAction(Player p);
}
