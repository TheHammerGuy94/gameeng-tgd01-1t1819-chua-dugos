﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawn : MonoBehaviour
{
    public GameObject prefab;
    public GameObject coinEx;
    public AudioClip coinPickup;
    private AudioSource src;

    public float spawnRate;
    private float proxRate;
    private float time = 0.00f;
    public float minX, maxX, minY, maxY;

    private List<Coin> coinList;
    private bool spawn = true;


    // Use this for initialization
    void Start()
    {
        coinList = new List<Coin>();
        proxRate = spawnRate;
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if( time >= proxRate){
            Spawn();
            time %= proxRate;
            time = Random.Range(0.5f, 1.0f) * Random.Range(spawnRate * 0.5f, spawnRate);
        }
    }

    public void DestroyedCoin(Coin c){
        //insert Code Here
        coinList.Remove(c);
    }
    public void SpawnExplosion(Vector2 v,int weight){
        GameObject o = Instantiate(coinEx);
        ParticleSystem p = o.GetComponent<ParticleSystem>();
        if (weight > 0){
            ParticleSystem.Burst m = p.emission.GetBurst(0);
            m.count = weight*2;
        }
        o.transform.position = v;
        p.Play();
        Destroy(o, p.main.duration);
    }

    public void Pickup(){
        src.PlayOneShot(coinPickup);
    }

    public void Spawn() {
        if (spawn)
        {
            GameObject o = Instantiate(prefab);
            Coin co = o.GetComponent<Coin>();
            co.spawner = this;
            co.SetPrefab(Random.Range(minX, maxX), Random.Range(minY, maxY), Random.Range(5f,20f));
            coinList.Add(co);
        }
        else
            CancelInvoke();
    }

    public void ActiveSpawn(bool spawn){
        this.spawn = spawn;

    }




}
