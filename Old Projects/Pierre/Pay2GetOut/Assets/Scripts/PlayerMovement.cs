﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerMovement : MonoBehaviour {
    private Rigidbody2D rb;
    protected Vector2 targetDirection = Vector2.zero;

    public float speed;

	// Use this for initialization
	void Start () {
        if(rb == null)
            rb = GetComponent<Rigidbody2D>();
        speed = 5.0f;
	}
	
	// Update is called once per frame
	void Update () {

        GetInput();
        if (targetDirection.magnitude > 0)
        {
            targetDirection.Normalize();
            rb.velocity = targetDirection * speed;
        }
        else
            rb.velocity = targetDirection; //this is zero by default;
	}
    public abstract void GetInput();

    public Vector2 MoveUp() {
        return Vector2.up;
    }
    public Vector2 MoveDown() {
        return Vector2.down;
    }
    public Vector2 MoveLeft() {
        return Vector2.left;
    }
    public Vector2 MoveRight() {
        return Vector2.right;
    }

}
