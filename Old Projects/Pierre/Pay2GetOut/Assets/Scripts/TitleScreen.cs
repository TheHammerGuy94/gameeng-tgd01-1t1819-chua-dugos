﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour {
    public TextAsset txt;
    public FastestTimeUpdate txtU;

	// Use this for initialization
	void Start () {

        if(txt == null)
            txt = Resources.Load<TextAsset>("gameSettings.txt");

        if (txt != null)
        {
            if(SceneSettings.GetInstance().GetSetting("gameLoaded").Length<=0){
                LoadSettings();
                SceneSettings.GetInstance().SetSetting("gameLoaded", "yes");
            }
        }
        txtU.time = float.Parse(SceneSettings.GetInstance().GetSetting("HighScore"));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadSettings(){
        string[] settingsList = txt.text.Split('\n');

        Debug.Log("settings loaded:" + settingsList.Length);

        if(settingsList.Length >0){
            for (int i = 0; i < settingsList.Length; i++){
                settingsList[i] = settingsList[i].Trim();
                if(settingsList[i].Length >0)
                    WriteSetting(settingsList[i]);
            }
        }


    }

    private void WriteSetting(string s){
        string[] setting = s.Split(',');
        SceneSettings.GetInstance().SetSetting(setting[0].Trim(), setting[1].Trim());
        Debug.Log("setting written: " + setting[0]);
    }

    public void LoadGame(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
