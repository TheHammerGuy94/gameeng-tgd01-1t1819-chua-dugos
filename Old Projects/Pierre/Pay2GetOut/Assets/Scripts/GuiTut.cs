﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiTut : MonoBehaviour {
    public Player p;
    public Text t;
	// Use this for initialization
	void Start () {
        if (p == null)
            p = Game.instance.p;
        t = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if (p.coins > 1)
            this.gameObject.SetActive(false);
	}
}
