﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    private Rigidbody2D rb;
    public int coins;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        coins = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddMass(float x) {
        rb.mass += x;
        coins++;
    }
    public float GetMass(){
        return rb.mass;
    }
    public float CoinEfficiency() {
        return coins == 0 ? 0 : rb.mass / (float)coins;
    }
}
