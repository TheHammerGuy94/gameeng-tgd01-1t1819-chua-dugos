﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Earthquake : MonoBehaviour {

    private Vector3 location;
    public float intensity;
    // Use this for initialization
    void Start()
    {
        location = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = location + Random.insideUnitSphere * intensity;

        //cos(long)* cos(lat), sin(lat),sin(long)*cos(lat)
    }
}
