﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastestTimeUpdate : GuiUpdate {
    public float time = -1f;
    private float defaultTime = -1f;
    public override void ExtraStart()
    {
        string s = SceneSettings.GetInstance().GetSetting("HighScore");
        if (s.Length > 0)
            time = float.Parse(s);


    }
    public override bool CheckPrevValue()
    {
        return time.CompareTo(defaultTime) != 0;
    }

    public override string GetValue()
    {
        return  defaultTime.ToString("f4");
    }

    public override void UpdateValue()
    {
        defaultTime = time;
    }
}
