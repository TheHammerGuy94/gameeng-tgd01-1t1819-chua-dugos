﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour
{
    public AudioClip intro, main;
    private AudioSource src;

    // Use this for initialization
    void Start()
    {
        src = GetComponent<AudioSource>();
        src.clip = intro;
        src.loop = false;
        src.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(!src.isPlaying && !src.loop)
        {
            src.clip = main;
            src.Play();
            src.loop = true;
        }

    }
}
