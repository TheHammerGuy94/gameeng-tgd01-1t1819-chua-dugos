﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour {
    public Text txt;
    private string format;
	// Use this for initialization
	void Start () {
        if(txt == null)
            txt = GetComponent<Text>();
        format = txt.text;
        txt.text = string.Format(format, SceneSettings.GetInstance().GetSetting("score"));
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.anyKeyDown){
            if (Input.GetKeyDown(KeyCode.Space))
                Replay();
            if (Input.GetKeyDown(KeyCode.Escape))
                MainMenu();
        }
	}

    public void MainMenu(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }


}
