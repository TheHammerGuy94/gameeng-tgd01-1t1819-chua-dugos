﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : PlayerMovement
{
    public override void GetInput()
    {
        targetDirection *= 0;
        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                this.targetDirection+=MoveUp();
            }
            if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                this.targetDirection += MoveDown();
            }
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                this.targetDirection += MoveRight();
            }
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                this.targetDirection += MoveLeft();
            }
        }

    }
}
