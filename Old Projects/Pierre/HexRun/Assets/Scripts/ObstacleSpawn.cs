﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawn : MonoBehaviour {
    public Obstacle o;
    public Player p;
    public float spawnAhead = 100f, destroyAhead=50f;
    private List<Obstacle> obstacles = new List<Obstacle>();
    public float spawnSpeed = 1.0f;
    private float timeProgress = 0.0f;

    public int sides = 8;
    public bool[] spawned;
    public float minHeight = 4.0f, maxHeight = 6.0f;
    public int maxTimes = 4;

	// Use this for initialization
	void Start () { 
        spawned = new bool[sides];
	}
	
	// Update is called once per frame
	void Update () {
        timeProgress += Time.deltaTime * spawnSpeed;

        if (timeProgress >= 1.0f) {
            timeProgress %= 1.0f;
            Spawn();
        }

        if (obstacles.Count > 0) {
            if (obstacles[0].transform.position.z <= p.transform.position.z - destroyAhead) {
                print("Destroying");
                Destroy(obstacles[0].gameObject);
            }
        }
	}

    public void Spawn() {
        int spawnCount = Random.Range(1, maxTimes + 1);
        ResetArray();
        for (int i = 0; i < spawnCount; i++) {
            int dir = Random.Range(0, 8);
            while (spawned[dir]){
                dir = Random.Range(0, 8);
            }
            spawned[dir] = true;
            Obstacle ox = Instantiate(o);
            ox.SpawnSettings(RHeight(), dir *RAngle(), p.transform.position.z + spawnAhead);
            ox.type = (ObsType)Random.Range(0, 3);
            ox.SetMaterial();
            ox.o = this;
            Destroy(ox.gameObject, 10f);

        }

    }

    private float RHeight() {
        return Random.Range(minHeight, maxHeight);
    }
    private float RAngle() {
        return (360 / sides) + Random.Range(-(360 / (sides * 4)), -(360 / (sides * 4)));
    }
    public void Destroyed(Obstacle o) {
        obstacles.Remove(o);
    }
    private void ResetArray() {
        for (int i = 0; i < spawned.Length; i++) {
            spawned[i] = false;
        }
    }

}
