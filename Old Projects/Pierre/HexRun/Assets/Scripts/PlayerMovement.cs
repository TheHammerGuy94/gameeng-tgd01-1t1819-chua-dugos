﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float playerSpeed = 10f;
    private float playerSpeedPrev = 0;
    public float jumpForce = 500f;
    public float hForce = 200f;
    public bool isAirborne = false;

    public Rigidbody rb;
    public TubeGravity gravity;
    private float targetRotation = 0f;
    private int curGround = 0,prevGround = 0;
    private ContinuousTube t;

	// Use this for initialization
	void Start () {
        if(rb == null)
            rb = GetComponent<Rigidbody>();
        if (gravity)
            gravity = GetComponent<TubeGravity>();
	}

    private void FixedUpdate()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, gravity.groundDir);
        if (Physics.Raycast(ray, out hit, 10f)) {
            if (hit.transform.gameObject.GetComponent<Ground>()) {
                Ground g = hit.transform.gameObject.GetComponent<Ground>();
                targetRotation = hit.collider.gameObject.transform.eulerAngles.z;
                curGround = hit.transform.gameObject.GetComponent<Ground>().groundIndex;
                if (g.t != t || t == null) {
                    t = g.t;
                    gravity.groundLoc = g.t.transform.position;
                }
            }
        }
    }

    // Update is called once per frame
    void Update () {
        float x = 0;
        if (playerSpeedPrev.CompareTo(playerSpeed) != 0) {
            playerSpeedPrev = playerSpeed;
        }
        if (Mathf.Abs(rb.velocity.z) != playerSpeedPrev) {
            rb.velocity = Vector3.forward * playerSpeedPrev + new Vector3(rb.velocity.x, rb.velocity.y, 0);
        }
        
        transform.eulerAngles = new Vector3(0, 0, Mathf.SmoothDampAngle(transform.eulerAngles.z, targetRotation, ref x, 0.1f));

        if (Input.anyKey) {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                SwitchLane(-1);
            }
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                SwitchLane(1);
            }
        }

        if (Input.anyKeyDown) {
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
            else if (Input.GetKeyDown(KeyCode.S)) {
                Drop();
            }
        }

	}

    public void SwitchLane(int x) {
        Debug.Log("switching lanes");
        gravity.SetDirectionVelocity(Mathf.Sin(Mathf.Rad2Deg *45)*(gravity.RotateGround(Vector2.right) * x * hForce +
                                    Vector3.forward * playerSpeed));
        //gravity.AddDirectionForce(gravity.RotateGround(Vector2.right) * x * hForce);
    }
    public void Jump() {
        if (!isAirborne) {
            Debug.Log("Jumping");
            gravity.AddDirectionVelocity(gravity.RotateGround(Vector3.up)* jumpForce);
            //gravity.AddDirectionForce(Vector3.up* jumpForce);
        }
    }
    public void Drop()
    {
        Debug.Log("Dropping");
        gravity.AddDirectionForce(gravity.TowardsGravity() * 50000);
    }

    private void OnCollisionEnter(Collision c)
    {
        if (c.collider.gameObject.tag == "Ground")
        {
            isAirborne = false;
        }
    }
    private void OnCollisionStay(Collision c)
    {
        if (c.collider.gameObject.tag == "Ground" && isAirborne)
        {
            isAirborne = false;
        }
    }
    private void OnCollisionExit(Collision c)
    {
        if (c.collider.gameObject.tag == "Ground")
        {
            isAirborne = true ;
        }
    }





}
