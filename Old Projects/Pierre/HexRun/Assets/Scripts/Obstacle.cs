﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Obstacle : MonoBehaviour {
    public ObstacleSpawn o;
    private Rigidbody rb;
    public TubeGravity grav;
    public bool hit = false;
    public ObsType type = ObsType.normal;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        grav = GetComponent<TubeGravity>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SpawnSettings(float height, float angle, float z) {
        transform.position = new Vector3(Mathf.Cos(Mathf.Deg2Rad * angle) * height, Mathf.Sin(Mathf.Deg2Rad * angle) * height, z);
        grav.multiplier = height <= 4.0f ? 1 : 0;

    }

    private void OnCollisionEnter(Collision c)
    {
        Player p = c.collider.GetComponent<Player>();
        if (p) {
            hit = true;
            //doSomething
            Destroy(this);
        }
    }

    public void SetMaterial() {
        switch (type) {
            case ObsType.dmg:
                GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().materials[1];
                break;
            case ObsType.heal:
                GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().materials[2];
                break;
            default:
                GetComponent<MeshRenderer>().material = GetComponent<MeshRenderer>().materials[0];
                break;
        }
    }
    private void OnDestroy()
    {
        
    }
}
