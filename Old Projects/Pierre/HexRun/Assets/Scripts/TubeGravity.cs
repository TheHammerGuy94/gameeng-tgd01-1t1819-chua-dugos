﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TubeGravity : MonoBehaviour {
    public float multiplier = 1;
    private Rigidbody rb;
    private Vector3 gravityMult = new Vector3(1f, 1f, 0);
    public Vector2 groundLoc = Vector3.zero;
    public Vector2 groundDir = Vector3.down;
    public Vector2 groundDirProx= Vector3.down;
    
    private Vector3 sumForces = Vector3.zero;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}

    private void FixedUpdate()
    {
        groundDir = groundLoc - (Vector2)transform.position;
        groundDirProx = groundDir.normalized;

        Debug.DrawLine(transform.position, transform.position + (Vector3)groundDirProx*2,Color.red);
        rb.AddForce(groundDir.normalized *Physics.gravity.magnitude * multiplier, ForceMode.Acceleration);

        if (sumForces.magnitude > 0f) {
            Debug.DrawLine(transform.position,
                (transform.position + sumForces).normalized*5
                );
            rb.AddForce(sumForces);
            sumForces *= 0;
        }
    }

    // Update is called once per frame
    void Update () {

	}

    public void AddDirectionForce(Vector3 dir) {
        sumForces += dir;
        Debug.DrawLine(transform.position, transform.position + dir.normalized * 2, Color.blue, 0.5f);
    }
    public void AddDirectionVelocity(Vector3 dir) {
        rb.velocity += dir;
    }
    public void SetDirectionVelocity(Vector3 dir) {
        rb.velocity = dir;
        Debug.DrawLine(transform.position, transform.position + dir.normalized * 2, Color.blue,0.5f);

    }
    public void ChangeDirection(Vector3 dir) {
        float mag = rb.velocity.magnitude;
        rb.velocity = dir.normalized * mag;

    }
    public Vector3 RotateGround(float degrees) {
        float angle = Mathf.Atan2(groundDir.y, groundDir.x) * Mathf.Rad2Deg;
        Debug.Log(string.Format("ground direction: {0}\nforce direction:{1}", angle, (angle + 90 + degrees)));
        return new Vector3(Mathf.Cos((angle +90 + degrees) * Mathf.Deg2Rad), Mathf.Sin((angle+90+degrees )* Mathf.Deg2Rad));
    }
    
    public Vector3 RotateGround(Vector3 relativeDir) {
        float angle = Mathf.Atan2(groundDir.y, groundDir.x) * Mathf.Rad2Deg;
        float targAngle = Mathf.Atan2(relativeDir.y, relativeDir.x) * Mathf.Rad2Deg;
        Debug.Log(string.Format("ground direction: {0}\nforce direction:{1}", angle, (angle + 90 + targAngle)));
        return new Vector3(Mathf.Cos((angle +90+targAngle) * Mathf.Deg2Rad), Mathf.Sin((angle + 90+targAngle) * Mathf.Deg2Rad));
    }
    public Vector3 TowardsGravity() {
        return groundDirProx;
    }
    
}
