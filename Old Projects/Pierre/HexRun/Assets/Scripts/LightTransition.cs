﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTransition : MonoBehaviour {
    public float cycleDuration = 60f;
    private float prev;
	// Use this for initialization
	void Start () {
        prev = transform.eulerAngles.x;
	}
	
	// Update is called once per frame
	void Update () {
        transform.eulerAngles = new Vector3((prev + Time.deltaTime / 60f * 360f) %360f, -50, 0);
	}
}
