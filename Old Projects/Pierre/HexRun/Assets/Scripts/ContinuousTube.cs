﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContinuousTube : MonoBehaviour {
    private Collider cd;
    public ContinuousTube next;
    public float offset = 400f;

	// Use this for initialization
	void Start () {
        cd = GetComponent<Collider>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void NextTube() {
        next.gameObject.transform.position = gameObject.transform.position + new Vector3(0, 0, offset);
    }

    private void OnTriggerEnter(Collider c)
    {
        if (c.GetComponent<Player>() != null) {
            NextTube();
        }
    }

    

}
