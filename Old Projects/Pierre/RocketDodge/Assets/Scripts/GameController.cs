﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    public static GameController instance = null;
    public bool isPaused = false, gameOver = false;
    public Flyer f;
    private int minLane, maxLane;

    public bool pausePrev = false;

    private void Awake(){
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);


       
    }

    // Use this for initialization
    void Start () {
        minLane = -2;
        maxLane = 2;

        if (f == null)
            f = GameObject.Find("Flyer").GetComponent<Flyer>();
    }
	
	// Update is called once per frame
	void Update () {
      

        if (pausePrev != isPaused) {
            Time.timeScale = isPaused ? 0 : 1;
            pausePrev = isPaused;
        }
       
        if (f.gas <= 0.0f) {
            RestartScene();
        }
    }

    public int getMinLane() {
        return this.minLane;
    }

    public int getMaxLane() {
        return this.maxLane;
    }
    public int getTotalLanes() {
        return getMaxLane() - getMinLane()+1;
    }

    public void GameOver() {

        gameOver = true;
    }

    public void RestartScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
