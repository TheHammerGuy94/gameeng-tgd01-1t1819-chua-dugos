﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectRefuel : ObstacleEffect{
    public float refuel = 0.25f;
    public override void EffectTrigger(Flyer f){
        gameObject.GetComponent<Renderer>().enabled = false;
        f.FlyerRefill(refuel);
    }
}
