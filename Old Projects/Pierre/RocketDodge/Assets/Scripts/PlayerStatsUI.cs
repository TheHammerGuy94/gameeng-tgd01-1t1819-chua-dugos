﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsUI : MonoBehaviour
{

    public Text score, health;
    private int scorePrev;
    private int healthPrev;
    public Flyer f;


    // Use this for initialization
    void Start()
    {
        scorePrev = -1;
        healthPrev = -1;
        if (score == null)
        {
            score = GameObject.Find("scoreUI").GetComponent<Text>();
        }
        if (health == null)
        {
            health = GameObject.Find("healthUI").GetComponent<Text>();
        }
        if (f == null)
        {
            f = GameObject.Find("Flyer").GetComponent<Flyer>();
        }

    }

    // Update is called once per frame
    void Update(){
        if (f.score != this.scorePrev) {
            UpdateScore();
        }
        if (Mathf.Floor(f.gas*20) != this.healthPrev) {
            UpdateHealth();
        }
    }

    private void UpdateScore() {
        string score = "Score:{0}";
        scorePrev = f.score;
        this.score.text = string.Format(score, scorePrev);

    }
    private void UpdateHealth() {
        Debug.Log("updating health");
        string health = "Health:|";
        this.health.text = health;
        this.healthPrev = (int)Mathf.Floor(f.gas * 20);
        for (int i = 0; i < 20; i++) {
            if (this.healthPrev >= i)
                this.health.text += "=";
            else
                this.health.text += " ";
        }
        this.health.text += "|";


    }
}
