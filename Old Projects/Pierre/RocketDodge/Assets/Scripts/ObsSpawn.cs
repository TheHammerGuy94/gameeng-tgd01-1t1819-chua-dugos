﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsSpawn : MonoBehaviour
{
    public GameObject dmgPreFab, gasPreFab;
    public float time = 0.0f;
    public float spawnDelay = 1.5f;

    void Spawn()
    {
        GameObject o = null;
        int x = Random.Range(GameController.instance.getMinLane(), GameController.instance.getMaxLane()+1);
        if (Random.value < 0.7f)
            o = Instantiate(dmgPreFab);
        else 
            o = Instantiate(gasPreFab);
        o.GetComponent<Obstacle>().lane = x;
        o.transform.position = new Vector2(10.0f, 0.0f);
        Destroy(o, 10f);
    }

	// Use this for initialization
	void Start()
    {
	}
	
	// Update is called once per frame
	void Update()
    {
        time = time + Time.deltaTime;

        if(time > spawnDelay)
        {
            time = time % spawnDelay;

            Spawn();
        }
	}
}
