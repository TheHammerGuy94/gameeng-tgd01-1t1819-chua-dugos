﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectDamage : ObstacleEffect{
    public override void EffectTrigger(Flyer f){
        f.FlyerHit();
    }
}
