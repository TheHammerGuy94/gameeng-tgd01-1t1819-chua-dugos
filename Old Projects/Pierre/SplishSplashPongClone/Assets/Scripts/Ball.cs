﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Ball : MonoBehaviour {
    private Collider2D cd;
    public Rigidbody2D rb;
    public GameObject miniSplode;
    public CameraShake cs;
    public float speed = 10;

    public int score = 0, life = 5;

    public bool shield = false;

    public AudioClip pong0, pong1;
    private AudioSource source;
    private int bounce = 0;

	// Use this for initialization
	void Start () {
        //pong0 = (AudioClip)Resources.Load("Audio/pong00.mp3");
        //pong1 = (AudioClip)Resources.Load("Audio/pong01.mp3");

        rb = GetComponent<Rigidbody2D>();
        cd = GetComponent<Collider2D>();
        source = GetComponent<AudioSource>();
        if (cs == null)
            cs = GameObject.Find("Main Camera shake").GetComponent<CameraShake>();
        rb.velocity = Vector2.left * speed;

	}
	
	// Update is called once per frame
	void Update () {
        if (Input.anyKey) {
            if (Input.GetKeyDown(KeyCode.Space) && Mathf.Abs(this.transform.position.x) < 5.7 && !Game.instance.paws)
                InvertDir();
            else if (Input.GetKeyDown(KeyCode.Q) && Game.instance.paws){
                Game.instance.gameOver = true;
                Game.instance.paws = false;
            }
        }
	}

    public void InvertDir() {
        rb.velocity = Vector2.Reflect(rb.velocity, Vector2.left);
    }

    public void AddScore(int x) {
        score += x;
    }
    public void AddScore() {
        score++;
        shield = Game.instance.diff == Difficulty.hard && (this.score > 30 && score % 10 == 0);
    }

    public void MinusLife(){
        cs.startShake();
        life--;
    }
    public void Explode(){
        Game.instance.spawnMan.Explosion(gameObject.transform.position);
        Destroy(gameObject);
    }
    public void Bounce(){
        Bounce(0.0f);
    }

    public void Bounce(float posX = 0.0f){
        bounce++;
        source.panStereo = posX / Game.maxX;
        switch (bounce % 2)
        {
            default: source.PlayOneShot(pong0, 1f); break;
            case 1: source.PlayOneShot(pong1, 1f); break;
        }

    }



}
