﻿using UnityEngine;
using System.Collections;

public class CoinSound : MonoBehaviour
{

    private AudioSource src;
    public AudioClip coinSound;

    // Use this for initialization
    void Start()
    {
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {}

    public void PlaySound(float x = 0.0f){
        src.panStereo = x / Game.maxX;
        src.PlayOneShot(coinSound);

    }
}
