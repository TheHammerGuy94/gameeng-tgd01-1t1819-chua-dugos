﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {
    private Collider2D cd;
    public float roteSpeed = 180;
    public Coin otherCoin;
    public CoinSound source;

	// Use this for initialization
	void Start () {
        cd = GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(0, roteSpeed * Time.deltaTime,0);
	}

    private void OnTriggerEnter2D(Collider2D c)
    {
        
        Debug.Log("Collision Detected");
        Ball b = c.gameObject.GetComponent<Ball>();
        if (b) {
            Debug.Log("Coin Picked up!!!");
            b.AddScore();
            source.PlaySound(this.transform.position.x);
            gameObject.SetActive(false);
            otherCoin.gameObject.SetActive(true);
        }else{
            Debug.Log("Ball not found");
        }
    }
}
