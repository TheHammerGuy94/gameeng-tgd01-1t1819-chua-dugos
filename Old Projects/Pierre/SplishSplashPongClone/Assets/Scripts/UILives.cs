﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILives : PlayerUI
{
    private int prevLives = -1;


    protected override void UpdateScore()
    {
        t.text = string.Format(this.s, prevLives);
    }

    protected override void UpdateValue()
    {
        prevLives = this.b.life;
    }

    protected override bool ValueCheck()
    {
        return prevLives != this.b.life;
    }
}
