﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSettings
{
    private static SceneSettings instance = null;
    public Dictionary<string, string> settings;

    private SceneSettings(){
        settings = new Dictionary<string, string>();
    }

    public static SceneSettings GetInstance(){
        if (instance == null) instance = new SceneSettings();
        return instance;
    }

    public string GetSetting(string setting){
        return (settings.ContainsKey(setting)) ? settings[setting] : "";
    }
    public void SetSetting(string setting, string value){
        if (settings.ContainsKey(setting))
            settings.Remove(setting);
        settings.Add(setting, value);
    }

}