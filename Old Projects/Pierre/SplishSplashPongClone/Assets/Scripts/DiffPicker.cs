﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DiffPicker : MonoBehaviour
{
    public ObsSpawner o;
    public AudioClip coin;
    private AudioSource src;

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1.0f;
        Difficulty d = DiffFuncs.ParseDifficulty(SceneSettings.GetInstance().GetSetting("difficulty"));
        o.SetSpawnRate(d,30);
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void pickEasy(){
        pickDiff(Difficulty.easy);
    }
    public void pickMedium()
    {
        pickDiff(Difficulty.medium);
    }
    public void pickHard()
    {
        pickDiff(Difficulty.hard);
    }

    public void pickDiff(Difficulty d){
        Debug.Log("player selected " + d.toString());
        SceneSettings.GetInstance().SetSetting("difficulty", d.ToString());
        src.PlayOneShot(coin);
        Invoke("LoadGame", 0.5f);

    }

    public void LoadGame(){
        SceneManager.LoadScene(1);
    }
}
