﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameState{
    playing, paused, gameOver
}
/*
    TODO:

    3)  Game states
    2)  implement highscore system (that means file processing duh!)
    1)  BGM and music
*/

public class Game : MonoBehaviour
{
    public static float maxX = 8.75f;
    public static Game instance = null;
    public Ball b;
    public Difficulty diff = Difficulty.easy;
    public ObsSpawnMan spawnMan;

    public bool paws = false, gameOver = false;
    private bool prevPaws = false, prevOver = false;
    public Text gameOverText, spacePrompt;
    public bool gameoverPress = false;
    public float prevScale = 1.0f;


    void Awake()
    {
        if (instance == null){
            instance = this;
            Debug.Log("initialized this");
        }
        else if (instance != this){

            Destroy(gameObject);
            Debug.Log("destryed thhis");
        }
        //DontDestroyOnLoad(gameObject);

    }

    // Use this for initialization
    void Start()
    {

        if (b == null)
            b = GameObject.Find("ball").GetComponent<Ball>();
        if (spawnMan == null)
            spawnMan = GameObject.Find("spawns").GetComponent<ObsSpawnMan>();
        if (gameOverText == null)
            gameOverText = GameObject.Find("gameOver").GetComponent<Text>();
        if(spacePrompt == null)
            spacePrompt = GameObject.Find("spacePrompt").GetComponent<Text>();

        gameOverText.gameObject.SetActive(false);
        spacePrompt.gameObject.SetActive(false);

        //load difficulty
        string diffs = SceneSettings.GetInstance().GetSetting("difficulty");
        if(diffs.Length >0)
            this.diff = DiffFuncs.ParseDifficulty(diffs);

        b.speed = DifficultySpeed();
        b.life = DifficultyLives(this.diff);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !prevOver)
                paws = !paws;
        }
        if (prevPaws != paws)
        {
            prevPaws = paws;
            pause();
        }
        if(b.life <= 0){
            gameOver = true;
        }
        if(prevOver != gameOver){
            prevOver = gameOver;
            GameOver();
        }
        if(prevOver && gameoverPress&& Input.anyKey){
            if(Input.GetKey(KeyCode.Space)){
                restartScene();
            }
            if(Input.GetKey(KeyCode.Escape)){
                TitleScreen();
            }
        }

    }

    public void pause()
    {
        Time.timeScale = prevPaws ? 0 : prevScale;
    }

    public void SetTimeScale(float scale){
        this.prevScale = scale;
        pause();
    }

    public float DifficultySpeed()
    {
        switch (GetDifficulty())
        {
            case Difficulty.easy: return 5;
            case Difficulty.medium: return 10;
            default: return 15;
        }
    }
    public int DifficultyLives(Difficulty d)
    {
        switch (d)
        {
            default: return 15;
            case Difficulty.hard: return 25;
        }
    }



    public static Difficulty GetDifficulty(){
        return GameExists() ? Game.instance.diff : DiffFuncs.ParseDifficulty(SceneSettings.GetInstance().GetSetting("difficulty"));
    }

    public static float TrackChance(){
        switch (GetDifficulty())
        {
            case Difficulty.easy: return 0.1f;
            case Difficulty.medium: return 0.15f;
            default: return 0.2f;
        }
    }

    public static float TrackSpeed()
    {
        switch (GetDifficulty())
        {
            case Difficulty.easy: return 1.1f;
            case Difficulty.medium: return 1.3f;
            default: return 1.5f;
        }
    }


    public static bool GameExists(){
        if (instance == null)
            return false;
        else
            return !instance.gameOver;

    }

    public void GameOver(){
        b.Explode();
        this.gameOver = true;
        spawnMan.Deactivate();

        showScreen();
    }
    public void restartScene(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void TitleScreen(){
        SceneManager.LoadScene(0);
    }
    public void showScreen(){
        ShowGameOver();
        Invoke("ShowSpacePrompt", 2.0f);
    }
    public void ShowSpacePrompt(){
        spacePrompt.gameObject.SetActive(true);
        gameoverPress = true;
    }
    public void ShowGameOver(){
        gameOverText.gameObject.SetActive(true);
    }
}

