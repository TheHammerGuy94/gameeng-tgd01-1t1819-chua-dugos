﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Difficulty
{
    easy, medium, hard
}

public static class DiffFuncs{
    public static string toString(this Difficulty d){
        switch(d){
            case Difficulty.easy: return "easy";
            case Difficulty.medium: return "medium";
            case Difficulty.hard: return "hard";
            default: return "";
        }
    }

    public static Difficulty ParseDifficulty(string e){
        if (e.ToLower() == "medium") return Difficulty.medium;
        else if (e.ToLower() == "hard") return Difficulty.hard;
        else return Difficulty.easy;
    }
}

public class ObsSpawner : MonoBehaviour {
    public ObsSpawnMan spawnMan;
    public GameObject obsPreFab;
    public float spawnTime = 1;
    public float time;
    public float spawnRate = 1;
    public float spawnMult = 1.0f;
    public bool variableSpawn = false;
    public bool multiply = false;
    public float minX = -4, maxX = 4;

    // Use this for initialization
	void Start () {
        variableSpawn = true;
        if (Game.instance.diff == Difficulty.easy || Game.instance.diff == Difficulty.hard)
            multiply = true;

        if (!variableSpawn)
            InvokeRepeating("Spawn", 0, spawnTime);
	}
	
	// Update is called once per frame
	void Update () {
        spawnRate = 1 / spawnTime;
        if(variableSpawn){
            time += Time.deltaTime;



            if (time >= spawnTime){
                time %= spawnTime;
                Spawn();
                for (int i = 1; i < (int)Mathf.Floor(spawnMult) && multiply; i++)
                    Spawn();
            }
        }
	}
    public void Spawn(){
        GameObject o = Instantiate(this.obsPreFab);
        Obstacle ox = o.GetComponent<Obstacle>();
        if (ox == null) {
            Debug.Log("noComponent found");
        }
        ox.SetSpawn(Random.Range(minX, maxX), this.transform.position.y, (this.transform.position.y>0));
        if (ox.track) {
            ox.InitializeTrack();
            ox.speed *= 1.5f;
        }
        if(Game.GameExists())
            spawnMan.AddSpawnedObstacle(ox);


        Destroy(o, 5f);
    }

    public void SetSpawnRate(Difficulty d, int score){
        spawnRate = DifficultyRate(d, score);
        Debug.Log(string.Format("spawn rate set to {0:00}",spawnRate));
        spawnTime = 1 / spawnRate;
        SetMultiplier();
    }
    private void SetMultiplier(){
        spawnMult = Game.GetDifficulty() == Difficulty.easy ? ((spawnRate - 1f) / 1.5f + 1f):1;

    }
    //note: just input the score. the function will do the rest
    public void SetSpawnRate(int score){
        SetSpawnRate(Game.instance.diff, score);
    }
    public static float DifficultyRate(Difficulty d, int score){
        switch (d){
            case Difficulty.easy :return (float)score / 20f + 0.5f;
            case Difficulty.medium: return (float)score / 40f + 1.0f;
            default: return DifficultyCurve(score);
        }

    }

    public static float DifficultyCurve(int score){
        return (score <= 0? 0.0f:3 * score * Mathf.Log10(score)) / (40 * Mathf.Log10(40)) + 0.5f;
    }
    public static float CombinedDifficulty(int score){
        return DifficultyRate(Difficulty.easy, score) + DifficultyRate(Difficulty.medium, score);
    }
}
