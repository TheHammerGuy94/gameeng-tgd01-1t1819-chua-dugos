﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsSpawnMan : MonoBehaviour {
    public ObsSpawner top, bot;
    private ObsSpawner tempDisable;
    private int prevScore = 0;
    public float spawnRate = -1;
    private float time;

    private List<Obstacle> list;
    private bool destroyMode = false;
    public AudioClip ex00, ex01;
    public GameObject miniSplode;
    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        setSpawnRates(0);
        disableOne();
        list = new List<Obstacle>();
        if(Game.instance.diff != Difficulty.easy)
            Invoke("Activate", ObsSpawner.DifficultyRate(Game.instance.diff,10)/2);
        source = GetComponent<AudioSource>();

    }
	// Update is called once per frame
	void Update () {
        if(prevScore != Game.instance.b.score){
            prevScore = Game.instance.b.score;
            setSpawnRates(prevScore);
            if(Game.instance.diff == Difficulty.hard){
                Game.instance.SetTimeScale((float)Game.instance.b.score / 80f + 1f);
            }
        }

        if(destroyMode && list.Count > 0){
            time += Time.deltaTime;
            if(time >= spawnRate){
                time %= spawnRate;
                DestroyObstacle();
            }
        }
	}

    public void setSpawnRates(int score){
        top.SetSpawnRate(score);
        bot.SetSpawnRate(score);
        spawnRate = Mathf.Max(top.spawnRate, bot.spawnRate);
    }

    private void disableOne(){
        tempDisable = (Random.value < 0.5f)? top: bot;
        tempDisable.gameObject.SetActive(false);
    }
    private void Activate(){
        tempDisable.gameObject.SetActive(true);
    }

    public void AddSpawnedObstacle(Obstacle b){
        list.Add(b);
    }



    public void Deactivate(){
        top.gameObject.SetActive(false);
        bot.gameObject.SetActive(false);
        destroyMode = true;

    }
    private void DestroyObstacle(){
        Obstacle o = list[Random.Range(0, list.Count)];
        o.Explode();
    }

    public void ExplodeSound(float posX = 0.0f){
        source.panStereo = posX / Game.maxX;
        switch (Random.Range(0,2))
        {
            default: source.PlayOneShot(ex00, 1f); break;
            case 1: source.PlayOneShot(ex01, 1f); break;
        }
    }

    public void Explosion(Vector2 v){

        GameObject o = Instantiate(miniSplode);
        o.transform.position = v;
        ParticleSystem p = miniSplode.GetComponent<ParticleSystem>();
        o.SetActive(true);
        ExplodeSound(this.transform.position.x);
        p.Play();
        Destroy(o, p.main.duration);

    }

    public void DestroyObstacle(Obstacle o){
        list.Remove(o);
    }


}
