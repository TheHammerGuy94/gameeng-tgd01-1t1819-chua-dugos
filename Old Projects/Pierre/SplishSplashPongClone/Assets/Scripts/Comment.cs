﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Comment : MonoBehaviour {
    public Text txt;
    private int prevScore = -1;
    private string current;

	// Use this for initialization
	void Start () {
        txt = GetComponent<Text>();
        current = GetCommentary(0);
	}
	
	// Update is called once per frame
	void Update () {
        if(prevScore != scoreState(Game.instance.b.score)){
            prevScore = scoreState(Game.instance.b.score);
            txt.text = GetCommentary(Game.instance.b.score);
        }


	}
    public void SetCustomCommentary(string s){
        txt.text = s;
    }

    public string GetCommentary(int score){

        switch (scoreState(score)){
            case 0: return startMsg();
            case 1: return tenPlus();
            case 2: return "if you don't know, pressing escape will pause the game.\n\tand while paused, the rage quit button is \'Q\'";
            case 3: return thirtyPlus();
            case 4: return fortyPlus();
            case 5: return "Fun fact\n\tThe DeveloPierre cannot get past 60+ points\n\tImpressive how you even got to 50+";
            default: return "";
        }
    }

    public int scoreState(int score){
        return score / 10;
    }
    public string startMsg(){
        switch (Game.instance.diff)
        {
            case Difficulty.easy: return "Press space to change Direction";
            case Difficulty.medium: return "That spawn rate counter on the top right?\n\tit only measures one spawn point...";
            case Difficulty.hard: return hardStart();
            default: return "";
        }

    }
    public string hardStart(){
        switch(Random.Range(0,4)){
            default: return "Since you selected Hard\n\tyou're confident on how to not die.";
            case 1: return "you see those lives?\n\t well, you're gonna need it!!!";
            case 2: return "you can blame the DeveloPierre for not including invicibility frames.";
            case 3: return "Believe it or not, an \"All homing arrows\" mode in this difficulty will make the game ironically easy.";
        }
    }

    public string tenPlus()
    {
        switch (Game.instance.diff)
        {
            case Difficulty.easy: return "If you think the DeveloPierre is an A-Hole for putting homing arrows in this game..\n\twell, you're right";
            case Difficulty.medium: return "It's a good thing those homing arrows have a limit to how far they can follow you...";
            case Difficulty.hard: return "Notice something different?\n\tyou guess...";
            default: return "";
        }

    }
    public string thirtyPlus(){
        switch (Game.instance.diff)
        {
            case Difficulty.easy: return "Getting tough?\n\twell that spawn rate counter will keep rising...";
            case Difficulty.medium: return "Too hard?\n\t Blame the dev of this game";
            case Difficulty.hard: return "There may or may not be a surprise when the score reaches 40...";
            default: return "";
        }

    }
    public string fortyPlus(){
        string x = "40+? dayum, that's high.\n";
        if (Random.value < 0.1f)
            x += "probably you too....XD";
        return x;
    }
}
