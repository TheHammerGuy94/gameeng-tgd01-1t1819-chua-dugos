﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    private static float minSize = 1.0f, maxSize = 2.0f;
    private static float minspeed = 2.5f, maxSpeed =5f;
    private static float minTrack = 1.5f;


    private Collider2D cd;
    private SpriteRenderer sp;
    private Vector2 target = Vector2.zero;
    public Rigidbody2D rb;
    public float speed = 2.5f;
    public bool track = false;
    public bool activeTrack = false;

	// Use this for initialization
	void Start () {
        cd = GetComponent<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
        sp = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        CheckTrack();
        if (track){
            if(activeTrack)
                UpdateTrack();
            TrackBall();
        }
        this.transform.eulerAngles = new Vector3(0, 0, GetRotation() - 90);
	}
    public void UpdateTrack(){
        if (Game.instance != null)
        if (!Game.instance.gameOver){
            target = (Vector2)Game.instance.b.transform.position+Game.instance.b.rb.velocity*Time.deltaTime*2;
        }
    }

    public void InitializeTrack(){
        UpdateTrack();
        activeTrack = Game.GameExists();

    }
    public void CheckTrack(){
        if (track && Mathf.Abs(rb.position.y) <= minTrack)
            track = false;
    }


    public void TrackBall(){

        Vector2 angle = (target - this.rb.position);
        angle.Normalize();
        rb.velocity = angle * speed;
    }

    public void SetSpawn(float x, float y, bool top)
    {
        float seed = Random.value;
        this.GetComponent<SpriteRenderer>().color = RandColor();
        this.rb.velocity = Vector2.up * SeededRange(seed,minspeed,maxSpeed) *(top?-1:1);
        this.track = Random.value < Game.TrackChance();
        this.rb.velocity *= track ? Game.TrackSpeed():1;
        this.transform.localScale *= SeededRange((1 - seed), minSize, maxSize);
        transform.position = new Vector2(x, y);

    }
    public static float SeededRange(float seed, float min, float max){
        return (max - min) * seed + min;
    }


    public void Explode(){
        Game.instance.spawnMan.Explosion(this.transform.position);
        Destroy(this);
    }

    public void OnTriggerEnter2D(Collider2D c){
        Ball b = c.GetComponent<Ball>();
        if (b){
            if (!b.shield) b.MinusLife();

            this.Explode();
            Destroy(gameObject);
        }
    }
    private float GetRotation(){
        return Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
    }

    private Color RandColor(){
        return Color.HSVToRGB(Random.value, 1.0f, 1.0f);
    }

    private void OnDestroy()
    {
        if(Game.GameExists())
            Game.instance.spawnMan.DestroyObstacle(this);
    }
}
