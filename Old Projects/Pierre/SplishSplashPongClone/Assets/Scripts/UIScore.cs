﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScore : PlayerUI {
    private int prevScore = -1;

    protected override bool ValueCheck(){
        return prevScore != this.b.score;
    }
    protected override void UpdateValue(){
        prevScore = this.b.score;

    }
    protected override void UpdateScore(){
        t.text = string.Format(this.s, this.b.score);

    }
}
