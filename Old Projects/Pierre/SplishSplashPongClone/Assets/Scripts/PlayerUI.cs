﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class PlayerUI : MonoBehaviour {
    public Ball b;
    protected Text t;
    protected string s;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
        s = t.text;
        UpdateScore();
	}
	
	// Update is called once per frame
	void Update () {
        if (ValueCheck()) {
            UpdateValue();
            UpdateScore();
        }
	}

    //checks if the alue has changed
    protected abstract bool ValueCheck();
    //updates said value
    protected abstract void UpdateValue();
    //updates the UI to that value
    protected abstract void UpdateScore();
}
