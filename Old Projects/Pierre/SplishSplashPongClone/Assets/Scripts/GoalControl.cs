﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalControl : MonoBehaviour {
    public Pong leftGoal, rightGoal;

	// Use this for initialization
	void Start () {
        if(leftGoal == null)
        {
            leftGoal = GameObject.Find("goalLeft").GetComponent<Pong>();
        }
        if (rightGoal == null)
        {
            rightGoal = GameObject.Find("goalRight").GetComponent<Pong>();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoalHit(Pong p){
        if(p == leftGoal){
            rightGoal.ActivateCoin();
        }
        else if(p == rightGoal){
            leftGoal.ActivateCoin();
        }
    }

}
