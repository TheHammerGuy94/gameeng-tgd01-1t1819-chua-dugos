﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pong : MonoBehaviour
{
    private Collider2D cd;
    public Coin cn;
    public GoalControl gc;

    // Use this for initialization
    void Start()
    {
        cd = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ActivateCoin(){
        cn.enabled = true;

    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        Ball b = c.gameObject.GetComponent<Ball>();
        if (b)
        {
            b.InvertDir();
            b.Bounce(transform.position.x);
            gc.GoalHit(this);
        }
    }

}
