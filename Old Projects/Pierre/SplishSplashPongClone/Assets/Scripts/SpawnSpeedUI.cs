﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnSpeedUI : MonoBehaviour {
    private float spawnSpeedPrev = -1.0f;
    private Text t;
    private string format;
	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
        format = t.text;
	}
	
	// Update is called once per frame
	void Update () {
        if(Game.instance.spawnMan.spawnRate!= spawnSpeedPrev){
            spawnSpeedPrev = Game.instance.spawnMan.spawnRate;
            UpdateText();
        }
	}

    public void UpdateText(){
        t.text = string.Format(format, spawnSpeedPrev.ToString("0.00"));
    }
}
