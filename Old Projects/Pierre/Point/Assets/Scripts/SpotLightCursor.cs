﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotLightCursor : MonoBehaviour {
    private Light light;
    public float minSpread = 10, maxSpread = 100;
    public float curSpread;
    public int progression;
    public Cursor c;
    private float intensity;
    private float fade = 1.0f;
    
	// Use this for initialization
	void Start () {
        light = GetComponent<Light>();
        curSpread = maxSpread;
        intensity = light.intensity;
	}

	// Update is called once per frame
	void Update () {

        if (Game.GameExists())
        {
            Vector3 rotation = Vector3.zero;
            rotation = (Game.PlayerPosition() - transform.position);
            rotation.Normalize();
            this.transform.rotation = Quaternion.LookRotation(rotation);
        }
        else {
            fade -= Time.deltaTime;
            if (fade <= 0.0f)
                fade = 0.0f;
            light.intensity = this.intensity * fade;
        }
	}

    public void SetRoundSpread(int i) {
        i--;
        float x = (float)(progression -i);
        curSpread = (i + 1 >= progression) ? minSpread : x / (float)progression * (maxSpread - minSpread) + minSpread;
        light.spotAngle = curSpread;
    }


}
