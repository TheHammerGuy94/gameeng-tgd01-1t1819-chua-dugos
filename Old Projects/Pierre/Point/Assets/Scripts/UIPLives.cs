﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPLives : UIPlayer {
    private int prevLives = -1;

    protected override void UpdateText()
    {
        t.text = string.Format(s, prevLives);
    }

    protected override void UpdateValue()
    {
        prevLives = p.lives;
    }

    protected override bool ValueCheck()
    {
        return p.lives != prevLives;
    }
}
