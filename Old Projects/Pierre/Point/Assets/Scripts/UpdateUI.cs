﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class UpdateUI : MonoBehaviour {
    protected Text t;
    protected string s;

	// Use this for initialization
	void Start () {
        t = GetComponent<Text>();
        s = t.text;
        UpdateText();
	}
	
	// Update is called once per frame
	void Update () {
        if (ValueCheck()) {
            UpdateValue();
            UpdateText();
        }
	}

    //checks if the value has changed
    protected abstract bool ValueCheck();
    //updates said value
    protected abstract void UpdateValue();
    //updates the UI to that value
    protected abstract void UpdateText();
}
