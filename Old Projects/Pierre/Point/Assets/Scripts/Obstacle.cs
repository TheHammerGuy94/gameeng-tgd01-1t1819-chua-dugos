﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    private static float minSize = 0.2f, maxSize = 0.4f;
    public static float minspeed = 2.5f, maxSpeed =5f;
    public static float minTrack = 1.5f;
    public SpawnMaster m = null;


    private Collider2D cd;
    private Vector3 target = Vector3.zero;
    public Rigidbody2D rb;
    public float speed = 3f;

    public bool track = true;
    public bool hit = false;

	// Use this for initialization
	void Start () {
        cd = GetComponent<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        UpdateTrack();
        track = track && CheckDistance();
        if (track)
            RotateDir();
    }

    public void SpawnSettings(float x, float y, Vector2 direction, float randomSeed) {
        SpawnSettings(new Vector2(x, y), direction, randomSeed);
    }

    public void SpawnSettings(Vector2 v, Vector2 direction, float randomSeed) {
        rb.velocity = direction * RandomSpeed(randomSeed);
        transform.position = v;
        this.gameObject.transform.position += new Vector3(0, 0, -0.1f);
        float size = RandomSize(randomSeed);
        this.gameObject.transform.localScale = new Vector3(size,size*2, 1f);
        GetComponent<SpriteRenderer>().color = Color.HSVToRGB(Random.value, 1, 1);

    }

    public bool CheckDistance() {
        return (target - transform.position).magnitude > minTrack;
    }

    public void RotateDir() {
        Vector3 vel = rb.velocity;
        Vector3 smooth = Vector3.SmoothDamp(transform.position, Game.PlayerPosition(), ref vel, Obstacle.minTrack  * Game.instance.SmoothFollow(), 5f, Time.smoothDeltaTime);
        Vector3 dir = (smooth - transform.position);
        dir.Normalize();
        rb.velocity = dir * speed;
        this.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg-90);
    }
    public bool CheckSpeed() {
        return rb.velocity.magnitude >= Obstacle.minspeed && rb.velocity.magnitude <= Obstacle.maxSpeed;
    }

    public void UpdateTrack() {
        // subject to change when player is gone
        Vector3 vel = rb.velocity;
        
        target = Game.PlayerPosition();
    }

    private float RandomSize(float seed) {
        return seed * (maxSize - minSize) + minSize;
    }
    private float RandomSpeed(float seed) {
        return (1.0f - seed )* (maxSize - minSize) + minSize;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //collide with player
        if (collision.gameObject.GetComponent<Player>())
            DmgPlayer(collision.gameObject.GetComponent<Player>());

    }

    private void OnTriggerExit2D(Collider2D c)
    {
        if (c.GetComponent<Game>()) {
            Destroy(gameObject);
            Debug.Log("Exited Game Bounds");
        }
    }

    public void DmgPlayer(Player p){
        Debug.Log("collided with player");
        if (!p.IsShield()) {
            p.AddLives(-1);
            hit = true;
        }
        Destroy(gameObject);
        
    }
    private void OnDestroy()
    {
        m.DestroyObstacle(this);
    }




}
