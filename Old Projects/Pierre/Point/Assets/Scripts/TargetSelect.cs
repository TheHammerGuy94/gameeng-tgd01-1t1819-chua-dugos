﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSelect : Target {
    private Collider2D cd;
    public SpawnTarget spawn;
    public int x, y;


    // Use this for initialization
    public new void Start () {
        base.Start();
        cd = GetComponent<Collider2D>();
        if (spriteColor)
            Debug.Log(string.Format("{0},{1} is not null", x, y));
    }
	
	// Update is called once per frame
	public new void Update () {
        
	}

    public void SetParent() {
        SpawnTarget t = transform.parent.gameObject.GetComponent<SpawnTarget>();
        if (t) this.spawn = t;
    }

    public void SelectTarget() {
        //game flow here
        if (spawn.colorGen.CheckColor(colorIndex))
        {
            Debug.Log("CORRECT!!! shuffling");
            spawn.colorGen.InitColors();
            spawn.ShuffleColors();
        }
        else
            Debug.Log("WRONG!!!");
    }

    public void PrintClick() {
        Debug.Log(string.Format("target: {0}", colorIndex));
    }
}
