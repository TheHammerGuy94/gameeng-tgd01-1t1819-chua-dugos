﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTarget : MonoBehaviour {
    public int rows = 10, columns = 10;
    public float minX = -5f, maxX = 5f;
    public float minY = -5f, maxY = 5f;

    public ColorGen colorGen;
    
    private TargetSelect[,] matrix;
    public GameObject prefab;
    public bool hasStarted = false;
    private int targI, targJ;


    // Use this for initialization
    void Start () {
        if (rows == 0 || columns == 0)
            rows = columns = Game.instance.GridSize();
        matrix = new TargetSelect[rows, columns];
        SpawnGrid();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void SpawnGrid() {
        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                GameObject o = Instantiate(prefab);
                TargetSelect t = o.GetComponent<TargetSelect>();
                o.transform.SetParent(gameObject.transform);
                // MAD HAXXOOOORR!!!!
                float x = minX + ColSpacing() / 2 + ColSpacing() * j;
                float y = minY + RowSpacing() / 2 + RowSpacing() * i;
                o.transform.position = new Vector2(x, y);
                o.transform.localScale = new Vector3(TargetSize(), TargetSize(), 1);
                //tentative
                t.gen = colorGen;
                t.x = i; t.y = j;
                matrix[i, j] = t;
                t.SetParent();
                Debug.Log(string.Format("target({0},{1})", matrix[i, j].x, matrix[i, j].y));
            }
        }
        hasStarted = true;
    }

    public void ShuffleColors() {
        AssignMatrix();
        ColorizeMatrix();
    }

    public void AssignMatrix() {

        for (int i = 0; i < matrix.GetLength(0); i++) {
            for (int j = 0; j < matrix.GetLength(1); j++) {
                if (matrix[i, j])
                {
                    matrix[i, j].SetColorindex(colorGen.RandomIndex());
                }
                else
                    Debug.Log(string.Format("target({0},{1}) dosent exist", i, j));
            }
        }
        targI = Random.Range(0, matrix.GetLength(0));
        targJ= Random.Range(0, matrix.GetLength(1));

        matrix[targI, targJ].colorIndex = colorGen.TargetIndex();
    }

    public void ColorizeMatrix() {
        for (int i = 0; i < matrix.GetLength(0); i++){
            for (int j = 0; j < matrix.GetLength(1); j++){
                Debug.Log(string.Format("target({0},{1})", i, j));
                matrix[i, j].Colorize();
            }
        }
    }
    public Target GetTarget() {
        return matrix[targI, targJ];
    }

    private float MatrixWidth() {
        return maxX - minX;
    }
    private float MatrixHeight() {
        return maxY - minY;
    }
    private float RowSpacing() {
        return MatrixHeight() / rows;
    }
    private float ColSpacing(){
        return MatrixWidth() / columns;
    }
    private float TargetSize() {
        return Mathf.Min(RowSpacing(), ColSpacing());
    }



}
