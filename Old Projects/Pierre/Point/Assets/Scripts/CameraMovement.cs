﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    private Vector2 direction = Vector2.zero;
    public float speed = 5f;
    public bool canMove = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (canMove) {
            this.transform.position += (Vector3)direction * speed * Time.smoothDeltaTime;
        }
	}
}
