﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {
    private Transform tf;
    private Vector3 originalLocation;
    public float shakeDuration = 3.0f, currentDuration = 0.0f;
    public float maxShake =6.0f;
    public float shakeIntensity = 5.0f;


	// Use this for initialization
	void Start () {
        tf = gameObject.GetComponent<Transform>();
        originalLocation = tf.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentDuration > 0.0f)
        {
            currentDuration -= Time.deltaTime;
            if(Time.timeScale >0)
                Shake();
            if(currentDuration < 0.0f){
                currentDuration = 0.0f;
            } 
        }

	}
    public void startShake(){
        currentDuration += shakeDuration;
        currentDuration = Mathf.Min(currentDuration, maxShake);
    }

    private void Shake(){
        tf.localPosition = originalLocation + Random.insideUnitSphere * shakeIntensity * (currentDuration / shakeDuration);
    }


}
