﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMaster : MonoBehaviour {
    public GameObject preFab;
    public GameObject miniSplode;
    public List<SpawnSlave> spawnPoints = new List<SpawnSlave>();
    public List<Obstacle> oList = new List<Obstacle>();
    public List<float> distanceList = new List<float>();
    private float maxDistance = 0.0f;
    public float spawnRate;
    public float time = 0;
    public float trackMin = 2f, trackMax = 5f;
    public int spawnMult = 3;

    public float ySize, xSize;
    public AudioClip ex0, ex1;
    private AudioSource src;

	// Use this for initialization
	void Start () {
        foreach (SpawnSlave s in spawnPoints)
            s.master = this;
        for (int i = 0; i < spawnPoints.Count; i++)
            distanceList.Add(1.0f);
        src = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime / spawnRate;
        CalculateDistances();
        if (time >= 1.0f) {
            time %= 1.0f;
            int x = Random.Range(1, spawnMult + 1);
            
            for (int i = 0; i < spawnMult; i++) {
                spawnPoints[NextWeight()].Spawn();
            }

            if (spawnMult == 0 && oList.Count > 1) {
                Destroy(oList[Random.Range(0, oList.Count)]);
            }

        }
        if (oList.Count > 1) {
            Obstacle o = oList[Random.Range(0, oList.Count)];
            if (!o.CheckSpeed())
                Destroy(o.gameObject);
        }

        
	}

    public void SpawnExplosion(Vector2 v) {
        GameObject o = Instantiate(miniSplode);
        o.transform.position = v;
        ParticleSystem s = o.GetComponent<ParticleSystem>();
        o.SetActive(true);
        s.Play();
        src.PlayOneShot(RandomClip());
        Camera.main.GetComponent<CameraShake>().startShake();
        Destroy(o, s.main.duration);
    }
    
    public AudioClip RandomClip() {
        switch (Random.Range(0, 2)) {
            default: return ex0;
            case 1: return ex1;
        }
    }
    
    public void DestroyObstacle(Obstacle o) {
        Vector2 ov = o.transform.position;
        if (o.hit) 
            SpawnExplosion(ov);
        oList.Remove(o);
    }

    public void NewRoundSpawnSettings(int round) {
        SetSpawnRate(round);
        SetTrackRange(round);

    }


    public void SetSpawnRate(int x) {
        float max = ((float)x / 50f);
        max = Mathf.Min(max, 1.0f);
        spawnRate = 1 /( max*5);

    }

    private void CalculateDistances() {
        maxDistance = 0.0f;
        for (int i = 0; i < spawnPoints.Count; i++) {
            distanceList[i] = spawnPoints[i].DistanceToCursor();
            maxDistance += distanceList[i];
        }
    }
    private float WeightedRandom() {
        return Random.Range(0.0f, maxDistance);
    }
    private int NextWeight() {
        float rand = WeightedRandom();
        int i = 0;
        while (rand > distanceList[i]) {
            rand-= distanceList[i];
            i++;
        }
        return i;
    }


    public void SetTrackRange(int x) {
        float range = 1.0f - ((float)x / (float)Game.instance.maxLevel);
        range = Mathf.Min(range, 1.0f);
        Obstacle.minTrack = range * (trackMax - trackMin) + trackMin;
    }
    public void StopSpawns() {
        spawnMult = 0;
        foreach (Obstacle o in oList) {
            o.track = false;
        }
    }

}
