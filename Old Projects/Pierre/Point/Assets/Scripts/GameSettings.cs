﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class GameSettings
{
    private static GameSettings instance = null;
    private Dictionary<string, string> dictionary = null;

    private GameSettings() {
        dictionary = new Dictionary<string, string>();
    }
    public static GameSettings GetInstance() {
        if (instance == null)
            instance = new GameSettings();

        return instance;
    }
    //place settings
    public void PlaceSetting(string key, string value) {
        if (dictionary.ContainsKey(key))
            dictionary.Remove(key);
        dictionary.Add(key, value);
    }

    public string GetSetting(string key) {
        return dictionary.ContainsKey(key) ? dictionary[key] : "";
    }



}

