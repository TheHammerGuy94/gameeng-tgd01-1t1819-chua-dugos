﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {
    public Player p;
    public AudioClip clip;
    public bool canAddLives = false;
    private AudioSource src;

	// Use this for initialization
	void Start () {
        src = GetComponent<AudioSource>();
        if (p == null)
            p= Game.instance.p;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D c)
    {
        Obstacle o = c.GetComponent<Obstacle>();
        if (o != null) {
            src.pitch = Random.Range(0.8f, 1.2f);
            src.PlayOneShot(clip);
            if (Random.value < 0.7f && canAddLives)
                p.AddLives(1);
            o.hit = false;
            Destroy(o.gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D c)
    {
        Obstacle o = c.GetComponent<Obstacle>();
        if (o != null)
        {
            o.hit = false;
            Destroy(o);
        }
    }
}
