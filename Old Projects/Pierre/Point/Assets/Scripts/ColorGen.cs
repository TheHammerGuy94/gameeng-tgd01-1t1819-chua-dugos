﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorGen : MonoBehaviour {
    public List<Color> colorList;
    public float variance = 5;
    public float maxVariance = 24;
    

	// Use this for initialization
	void Start () {
        colorList = new List<Color>();
	}
	
	// Update is called once per frame
	void Update () {	}

    public void SetRoundVariance(int round) {
        round--;
        variance = (float)round / (float)Game.instance.maxLevel;
        variance = Mathf.Min(variance, 1.0f);
        variance = variance*(maxVariance - 5)+5;
    }

    public void InitColors() {
        int varColor = (int)Mathf.Floor(variance);
        colorList.Clear();
        float x = Random.value;
        for (int i = 0; i < varColor; i++) {
            colorList.Add(Color.HSVToRGB((x + (float)i / (float)varColor)%1.0f, 1, 1));
        }
        Debug.Log(string.Format("there are {0} Colors", colorList.Count));
        ShuffleColors();
    }

    private void ShuffleColors() {
        for (int i = 0; i < colorList.Count; i++) {
            int j = Random.Range(i, colorList.Count);
            Color temp = colorList[i];
            colorList[i] = colorList[j];
            colorList[j] = temp;
        }
    }

    public int RandomIndex() {
        return (colorList.Count > 0) ? Random.Range(0, colorList.Count-1) : -1;
    }
    public int TargetIndex() {
        return colorList.Count - 1;
    }

    public Color GetColor(int i) {
        return (i >= 0 && i < colorList.Count) ? colorList[i] : Color.white;
    }

    public bool CheckColor(int i) {
        return i == colorList.Count - 1;
    }
}
