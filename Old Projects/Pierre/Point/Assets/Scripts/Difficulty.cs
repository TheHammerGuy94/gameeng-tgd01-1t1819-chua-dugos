﻿using System.Collections;
using System.Collections.Generic;

public enum Difficulty{
    easy, medium, hard
}

public static class DifficultyFuncs {
    public static Difficulty ParseDifficulty(string x) {
        x = x.ToLower();
        if (x == "medium") return Difficulty.medium;
        else if (x == "hard") return Difficulty.hard;
        else return Difficulty.easy;
    }

    public static string ToString(this Difficulty x) {
        switch (x) {
            case Difficulty.medium  : return "medium";
            case Difficulty.hard: return "hard";
            default: return "easy";
        }

    }
}
