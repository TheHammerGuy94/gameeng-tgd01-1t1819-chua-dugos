﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveState {
    find,returning, home
}

public class BaconSearch : MonoBehaviour {
    private Target destTarget = null;
    public Cursor c;
    public float distance = 5.0f;
    public float speenSpid = 0.5f, rotate = 0f; 

	// Use this for initialization
	void Start () {
        if (!c)
            c = Game.instance.p.c;

        if (!destTarget)
            this.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (WithinRange())
            Spin();
        else
            Point();
        transform.eulerAngles = new Vector3(0, 0, rotate - 90);
        transform.position = new Vector3(Mathf.Cos(rotate * Mathf.Deg2Rad)* distance, Mathf.Sin(rotate * Mathf.Deg2Rad) * distance, 0) + Game.PlayerPosition();

    }
    public void Spin() {
        rotate += 360 / speenSpid * Time.deltaTime;
        rotate %= 360f;

    }
    public void Point() {
        Vector2 f = Cursor2Target();
        f.Normalize();
        rotate = Mathf.Atan2(f.y, f.x) * Mathf.Rad2Deg;
        
    }


    public void SetTarget(Target t) {
        destTarget = t;
    }

    public Vector2 Cursor2Target() {
        return GetTargetV() - (Vector2)Game.PlayerPosition();
    }

    private Vector2 GetTargetV() {
        return destTarget != null ? (Vector2)destTarget.transform.position : Vector2.zero;
    }

    public bool WithinRange() {
        return Cursor2Target().magnitude <= destTarget.transform.localScale.x;
    }

    public bool WithinRange(Target t, Cursor c) {
        return ( t.transform.position -Game.PlayerPosition() ).magnitude <= t.transform.localScale.x;
    }
}
