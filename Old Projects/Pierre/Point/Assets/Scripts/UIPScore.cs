﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPScore : UIPlayer{
    private int prevScore = -1;
    
    protected override void UpdateText()
    {
        t.text = string.Format(s, prevScore);
    }

    protected override void UpdateValue()
    {
        prevScore = p.score;
    }

    protected override bool ValueCheck()
    {
        return p.score != prevScore;
    }

}
