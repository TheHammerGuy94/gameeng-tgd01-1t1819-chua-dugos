﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSlave : MonoBehaviour {
    public SpawnMaster master;
    public Vector2 direction;
    public Vector2 minLoc, maxLoc;
    

	// Use this for initialization
	void Start () {
        direction = Vector2.zero - (Vector2)this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {}

    public void Spawn() {
        GameObject o = Instantiate(master.preFab);
        Obstacle ox = o.GetComponent<Obstacle>();
        if (ox)
        {
            ox.SpawnSettings(Vector2Range(), direction.normalized, Random.value);
            ox.m = master;
            master.oList.Add(ox);
        }
        else Destroy(o);

    }

    public float DistanceToCursor() {
        Vector2 x = Game.PlayerPosition() - this.transform.position;
        x.Scale(new Vector2(direction.x*2/(master.xSize*master.xSize), direction.y*2/(master.ySize * master.ySize)));
        return x.magnitude;
    }

    public Vector2 Vector2Range() {
        return new Vector2(Random.Range(minLoc.x, maxLoc.x)
            , Random.Range(minLoc.y, maxLoc.y));
    }


}
