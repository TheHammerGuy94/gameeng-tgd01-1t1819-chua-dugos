﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour {
    private Vector2 cursorLoc = Vector2.zero;
    public float speed = 10;
    public int colorCode = 0;
    public bool controller = false;

	// Use this for initialization
	void Start () {
        controller = controller && Game.instance.controllerAvail;
        UpdateLocation();
    }

    void FixedUpdate()
    {
        if(!Game.instance.pause)
            UpdateLocation();
        
        RaycastHit2D[] hits = Physics2D.RaycastAll(this.transform.position, Vector2.down);
        TargetSelect t = null;
        for (int i = 0; i < hits.Length && !t ; i++) {
            t = hits[i].collider.gameObject.GetComponent<TargetSelect>();
        }
        Game.instance.SelectTarget(t);
    }

    // Update is called once per frame
    void Update () {
        //get direction to cursor
        transform.position = new Vector3(transform.position.x, transform.position.y, -0.1f);
        

    }

    private void UpdateLocation() {
        if (controller)
            ControllerUpdate();
        else
            MouseUpdate();
    }

    private void MouseUpdate() {
        cursorLoc = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = Vector3.MoveTowards(transform.position, cursorLoc, speed * Time.deltaTime);
    }

    private void ControllerUpdate() {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        transform.position += (Vector3)new Vector2(x, y) * speed * Time.smoothDeltaTime;
    }


}
