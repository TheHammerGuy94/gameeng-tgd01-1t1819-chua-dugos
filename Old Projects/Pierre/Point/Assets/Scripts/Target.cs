﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target: MonoBehaviour {
    public SpriteRenderer spriteColor;
    public ColorGen gen;
    public int colorIndex = -1;
    private int tempColor = -1;
    public Animator am = null;

	// Use this for initialization

	public void Start () {
        spriteColor = GetComponent<SpriteRenderer>();
        if (gen == null)
            gen = Game.instance.gen;
        if (am == null)
            am = gameObject.GetComponent<Animator>();

        if (am != null)
            Debug.Log("Animator loaded");
        else
            Debug.Log("Animator nope");
        Debug.Log(spriteColor.ToString());
    }
	
	// Update is called once per frame
	public void Update () {
	}
    public void SetColorindex(int i) {
        colorIndex = i;
    }

    public void Colorize(){
        //*/
        spriteColor.color = gen.GetColor(colorIndex);
        /*/
        tempColor = colorIndex;
        colorIndex = -1;
        am.Play("FadeOut");
        Invoke("ReColor", am.speed);
        //*/

    }

    private void ReColor() {
        spriteColor.color = gen.GetColor(tempColor);
        am.Play("FadeIn");
        Invoke("UnTemp", am.speed);
    }
    private void UnTemp() {
        colorIndex = tempColor;
    }
    public void Uncolor() {
        spriteColor.color = Color.clear;
    }


}
