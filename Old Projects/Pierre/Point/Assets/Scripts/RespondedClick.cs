﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespondedClick : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    
    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0)) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast((Vector2)ray.GetPoint(10), Vector2.down);

            if (hit.collider) {
                RespondedClick r =  hit.collider.gameObject.GetComponent<RespondedClick>();
                r.PrintClick();
            }

        }
    }

   

    void Update () {
		
	}

    void PrintClick() {
        Debug.Log("Clicked!!!"+ this.transform.position);
    }
}
