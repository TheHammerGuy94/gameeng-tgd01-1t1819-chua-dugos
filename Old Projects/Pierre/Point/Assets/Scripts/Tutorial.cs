﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour {
    public Text txt;
    public string text0, text1;
	// Use this for initialization
	void Start () {
        if (txt == null)
            txt = GetComponent<Text>();
        txt.text = text0;
	}
	
	// Update is called once per frame
	void Update () {
        if (Game.instance.round > 0)
            txt.text = text1;
        if (Game.instance.round > 5)
            gameObject.SetActive(false);
	}
}
