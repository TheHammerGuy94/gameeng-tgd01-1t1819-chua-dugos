﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetSpawn : MonoBehaviour {
    public int rows = 10, columns = 10;
    public float minX = -5f, maxX = 5f;
    public float minY = -5f, maxY = 5f;

    public ColorGen colorGen;

    private Target[,] matrix;
    public GameObject prefab;

    private void Awake()
    {
        matrix = new Target[rows, columns];
    }

    // Use this for initialization
    void Start () {
        for (int i = 0; i < matrix.GetLength(0); i++) {
            for (int j = 0; j < matrix.GetLength(1); j++) {
                GameObject o = Instantiate(prefab);
                // MAD HAXXOOOORR!!!!
                float x = minX + ColSpacing() / 2 + ColSpacing() * j;
                float y = minY + RowSpacing() / 2 + RowSpacing() * i;
                o.transform.position = new Vector2(x, y);
                o.transform.localScale = new Vector3(TargetSize(), TargetSize(),1);
                o.transform.SetParent(gameObject.transform);
                //tentative
                TargetSelect t = o.GetComponent<TargetSelect>();
                t.x = i; t.y = j;
                t.SetParent();
                matrix[i,j] = t;
            }
        }
        ShuffleColors();
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ShuffleColors() {
        AssignMatrix();
        ColorizeMatrix();
    }

    public void AssignMatrix() {
        for (int i = 0; i < matrix.GetLength(0); i++) {
            for (int j = 0; j < matrix.GetLength(1); j++) {
                matrix[i, j].colorIndex = colorGen.RandomIndex();
            }
        }
        matrix[Random.Range(0, matrix.GetLength(0)), Random.Range(0, matrix.GetLength(1))].colorIndex = colorGen.TargetIndex();
        
    }

    public void ColorizeMatrix() {
        for (int i = 0; i < matrix.GetLength(0); i++){
            for (int j = 0; j < matrix.GetLength(1); j++){
                matrix[i, j].Colorize();
            }
        }
    }

    private float MatrixWidth() {
        return maxX - minX;
    }
    private float MatrixHeight() {
        return maxY - minY;
    }
    private float RowSpacing() {
        return MatrixHeight() / rows;
    }
    private float ColSpacing(){
        return MatrixWidth() / columns;
    }
    private float TargetSize() {
        return Mathf.Min(RowSpacing(), ColSpacing());
    }



}
