﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
    public float timer,maxTimer;
    public int points = 5;
    public int progression = 50;
    public float minTime = 10f, maxTime = 25f;

	// Use this for initialization
	void Start () {
        timer = 0;
	}
	// Update is called once per frame
	void Update () {
        if(Game.GameExists())
            timer -= Time.deltaTime;

        if (timer <= 0) {
            timer = 0;
            if(Game.instance.round>0)
                Game.instance.StartSearch();
        }
	}

    public int GetPoints() {
        float percent = (timer / maxTimer);
        return (percent > 0.9f)? points*2 : Mathf.CeilToInt((float)(points - 1) * percent) + 1;
    }
    public void NewRoundTimer(int i) {
        i--;
        float x = (float)i;
        maxTimer = ((float)x / (float) Game.instance.maxLevel) * (maxTime - minTime) + minTime;
        timer = maxTimer;
    }
    
}
