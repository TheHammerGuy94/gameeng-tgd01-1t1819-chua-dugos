﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public int maxLives = 50;
    public int lives = 50,score = 0;
    private float shieldDuration = 1.0f;
    private float shieldTime = 0.0f;
    public Cursor c;
    public Shield s;
    public GameObject w;
    public AudioClip scoreSnd, wrong;
    public AudioSource src;
    private float wrongTime = 0;

	// Use this for initialization
	void Start () {
        if (!c)
            c = GetComponent<Cursor>();
        if (!s)
            s = transform.GetComponentInChildren<Shield>(true);
        if (!w)
            w = transform.Find("wrong").gameObject;
        if (!src)
            src = GetComponent<AudioSource>();
        
        s.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        if (wrongTime >= 0.0f) {
            wrongTime -= Time.deltaTime;
            if (wrongTime <= 0) {
                wrongTime = 0;
                w.SetActive(false);
            } 
        }

        if (IsShield()) {
            shieldTime -= Time.deltaTime;
            if (shieldTime <= 0) {
                shieldTime = 0;
                s.gameObject.SetActive(false);
            }
        }
	}
    public void StartShield() {
        if (!IsShield()) {
            shieldDuration = Game.instance.t.maxTimer / 10f;
            shieldTime += shieldDuration;
            s.canAddLives = lives <= maxLives * 0.5f;
            s.gameObject.SetActive(true);
            //then activate some shield animation...
        }
    }
    public bool IsShield() {
        return shieldTime > 0.0f;
    }
    public void StartWrong() {
        wrongTime = 1.0f;
        src.PlayOneShot(wrong);
        w.SetActive(true);
    }

    public void AddLives(int i) {
        lives += i;
        lives = Mathf.Min(lives, maxLives);
        lives = Mathf.Max(lives, 0);
        
        if (lives <= 0)
            Game.instance.gameOver = true;
    }
    public void AddScore(int x) {
        score += x;

        StartShield();
        src.PlayOneShot(scoreSnd);
    }
}
