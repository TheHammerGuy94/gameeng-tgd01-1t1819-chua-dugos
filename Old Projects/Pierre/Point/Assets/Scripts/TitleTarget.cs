﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleTarget : MonoBehaviour {
    public float minX, maxX, minY, maxY;

	// Use this for initialization
	void Start () {
		
	}

    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0)){
            Debug.Log("click");
            RaycastHit2D x = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.down);
            if (x) {
                x.collider.gameObject.GetComponent<TitleTarget>().Teleport();
            }
        }
    }

    private void OnMouseEnter()
    {
        Debug.Log("teleport");
        GetComponent<SpriteRenderer>().color = Color.HSVToRGB(Random.value, Random.value, 1.0f);
        this.transform.position = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
    }

    // Update is called once per frame
    void Update () {
        
	}
    public void Teleport() {
    }
}
