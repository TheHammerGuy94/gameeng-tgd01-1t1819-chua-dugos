﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class Game : MonoBehaviour {
    public static Game instance = null;
    public SpawnTarget spawn;
    public SpawnMaster master;
    public ColorGen gen;
    public Timer t;
    public Target findTarget,selectedTarget;
    public SpotLightCursor lt;
    public BaconSearch bacon;
    public Player p;
    public Difficulty diff = Difficulty.easy;

    public int round = 0;
    public int maxLevel = 50;
    public bool controllerAvail = false;
    public bool pause = false;
    public bool gameOver = false;
    private bool prevGame = false;

    public Text textGame, textPress;
    private bool restart = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (this != instance)
            Destroy(gameObject);

        //get dificulty
        string x = GameSettings.GetInstance().GetSetting("difficulty");
        if (x.Length > 0)
            diff = DifficultyFuncs.ParseDifficulty(x);
        //if controller
        controllerAvail = Input.GetJoystickNames().Length > 0;
        x = GameSettings.GetInstance().GetSetting("joy");
        if (x.Length > 0)
            p.GetComponent<Cursor>().controller = controllerAvail && bool.Parse(x);


    }
    // Use this for initialization}
    void Start () {
        if (!spawn)
            spawn = GameObject.Find("targetGrid").GetComponent<SpawnTarget>();
        spawn.rows = spawn.columns = GridSize();
        if (!gen)
            gen = spawn.colorGen;
        if (!t)
            t = GetComponent<Timer>();
        if(!p)
            p = GameObject.Find("Cursor").GetComponent<Player>();
        p.maxLives = p.lives = PlayerLives();
        //then set difficulty
        if(!master)
            master = GameObject.Find("ObstacleSpawnMaster").GetComponent<SpawnMaster>();
        master.spawnMult = SpawnCount();
        if (!bacon)
            bacon = GameObject.Find("Bacon").GetComponent<BaconSearch>();

       


	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.anyKeyDown){
            if (!prevGame)
            {
                if (!pause)
                {
                    if ((Input.GetAxis("Fire") > 0.0f || Input.GetAxis("FireJoy") > 0.0f))
                    {
                        Debug.Log("Click");
                        SelectColor();
                    }
                    else if (Input.GetAxis("pause") > 0.0f)
                    {
                        TogglePause();
                    }
                }
                else if (pause)
                    TogglePause();
            }
        }
        if (prevGame != gameOver) {
            prevGame = gameOver;
            GameOver();
        }
        if (!GameExists()) {
            //show gameOver screen and Retry button
            Invoke("ShowGameOverScreen", 0.5f);
        }
        if (restart) {
            if ((Input.GetAxis("Fire") > 0.0f || Input.GetAxis("FireJoy") > 0.0f))
            {
                RestartScene();
            }
            else if ((Input.GetAxis("pause") > 0.0f)) {
                QuitGame();
            }
        }
	}
    public void SelectColor() {
        if (round < 1)
            NewRound();
        else
        {
            if (selectedTarget.colorIndex >= 0 && selectedTarget.colorIndex < gen.colorList.Count)
            {
                if (findTarget.colorIndex == selectedTarget.colorIndex)
                {
                    p.AddScore(t.GetPoints());
                    p.AddLives(1);
                    NewRound();
                }
                else
                {
                    p.AddLives(-2);
                    p.StartWrong();
                }
            }
        }
    }

    public void TogglePause() {
        Debug.Log("Paused");
        pause = !pause;
        Time.timeScale = pause ? 0 : 1;
    }

    public void NewRound() {
        //round++
        round++;
        //colorGen make new colors and shuffles
        gen.SetRoundVariance(round);
        gen.InitColors();
        lt.SetRoundSpread(Mathf.FloorToInt((float)round / 2.0f));
        if (!spawn.hasStarted)
            spawn.SpawnGrid();
        spawn.ShuffleColors();
        t.NewRoundTimer(round);
        findTarget.colorIndex = gen.TargetIndex();
        findTarget.Colorize();
        bacon.SetTarget(spawn.GetTarget());
        bacon.gameObject.SetActive(false);
        master.NewRoundSpawnSettings(round);
        //setTrackingRange
        if (!master.isActiveAndEnabled)
            master.gameObject.SetActive(true);
    }

    public void GameOver() {

        master.StopSpawns();
        bacon.gameObject.SetActive(false);
        Destroy(p.gameObject);
    }
    private void ShowGameOverScreen() {
        SaveSettings();
        textGame.gameObject.SetActive(true);
        textPress.gameObject.SetActive(true);
        restart = true;
    }

    public void SaveSettings() {
        GameSettings.GetInstance().PlaceSetting("difficulty", diff.ToString());
        GameSettings.GetInstance().PlaceSetting("highscore", p.score.ToString());
    }
    public void RestartScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex-1);
    }



    public static bool GameExists() {
        return Game.instance != null && !Game.instance.gameOver;
    }
    public static Vector3 PlayerPosition() {
        return GameExists() ? Game.instance.p.transform.position : Vector3.zero;
    }

    public void StartSearch() {
        bacon.gameObject.SetActive(true);
    }

    public void SelectTarget(Target t) {
        if (t)
        {
            selectedTarget.colorIndex = t.colorIndex;
            selectedTarget.Colorize();
        }
        else {
            selectedTarget.colorIndex = -1;
            selectedTarget.Uncolor();
        }
    }

    #region DIFFICULTY SETTINGS
    //player Lives
    public int PlayerLives() {
        return (GridSize() *GridSize())/2;
    }

    //gridSize

    public int GridSize() {
        switch (diff) {
            default: return 8;
            case Difficulty.medium: return 10;
            case Difficulty.hard: return 12;
        }
    }

    //roound progression
    public int MaxRound() {
        switch (diff)
        {
            default: return 200;
            case Difficulty.medium: return 100;
            case Difficulty.hard: return 100;
        }
    }

    //spawnMultiplier
    public int SpawnCount()
    {
        switch (diff)
        {
            default: return 1;
            case Difficulty.medium:
            case Difficulty.hard: return 2;
        }
    }
    public float SmoothFollow() {
        switch (diff)
        {
            default: return 2;
            case Difficulty.medium: return 1;
            case Difficulty.hard: return 0.5f;
        }
    }
    #endregion
}
