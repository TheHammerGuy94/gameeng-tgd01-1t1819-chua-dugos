﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitleDifficuly : MonoBehaviour {
    public Toggle t;
    private bool controllerAvail = false;

	// Use this for initialization
	void Start () {
        CheckController();
    }
	
	// Update is called once per frame
	void Update () {
        t.interactable = controllerAvail;
        t.isOn = t.isOn && controllerAvail;
        CheckController();
	}

    public void CheckController() {
        controllerAvail = Input.GetJoystickNames().Length > 0;
    }

    public void SelectEasy() {
        SelectDifficulty(Difficulty.easy);
    }
    public void SelectMedium() {
        SelectDifficulty(Difficulty.medium);
    }

    public void SelectHard() {
        SelectDifficulty(Difficulty.hard);
    }

    public void SelectDifficulty(Difficulty d) {
        GameSettings.GetInstance().PlaceSetting("difficulty", d.ToString());
        GameSettings.GetInstance().PlaceSetting("joy", t.isOn.ToString());
        Invoke("LoadGame", 1.0f);
    }

    public void LoadGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }


}
