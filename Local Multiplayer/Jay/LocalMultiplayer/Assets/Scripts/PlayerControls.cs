﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    public float horizontalSpeed;
    public float jumpPower;

    public int playerNumber;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        switch (playerNumber)
        {
            case 1:
            {
                if (Input.GetKey(KeyCode.A))
                {
                    transform.position = transform.position + (new Vector3(-1.0f, 0, 0)) * horizontalSpeed * Time.deltaTime;
                }

                if (Input.GetKey(KeyCode.D))
                {
                    transform.position = transform.position + (new Vector3(1.0f, 0, 0)) * horizontalSpeed * Time.deltaTime;
                }

                if (Input.GetKey(KeyCode.W))
                {
                    transform.position = transform.position + (new Vector3(0, 1.0f, 0)) * jumpPower * Time.deltaTime;
                }
            }

            break;

            case 2:
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    transform.position = transform.position + (new Vector3(-1.0f, 0, 0)) * horizontalSpeed * Time.deltaTime;
                }

                if (Input.GetKey(KeyCode.RightArrow))
                {
                    transform.position = transform.position + (new Vector3(1.0f, 0, 0)) * horizontalSpeed * Time.deltaTime;
                }

                if (Input.GetKey(KeyCode.UpArrow))
                {
                    transform.position = transform.position + (new Vector3(0, 1.0f, 0)) * jumpPower * Time.deltaTime;
                }
            }

            break;

            default:
            {

            }

            break;
        }
    }
}