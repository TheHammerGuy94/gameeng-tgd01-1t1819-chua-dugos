﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpForcePowerUp : MonoBehaviour
{
    private float jumpForceMultiplier = Mathf.Sqrt(2.0f);
	   
    // Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
		
	}

    void OnTriggerEnter2D(Collider2D collidedWith)
    {
        if(collidedWith.GetComponent<Player>() != null)
        {
            float timeRemaining = 2.0f;
            float originalJumpForce = collidedWith.GetComponent<Player>().jumpForce;
            float modifiedJumpForce = collidedWith.GetComponent<Player>().jumpForce * jumpForceMultiplier;

            collidedWith.GetComponent<Player>().jumpForce = modifiedJumpForce;

            timeRemaining = timeRemaining - Time.deltaTime;

            if(timeRemaining <= 0)
            {
                collidedWith.GetComponent<Player>().jumpForce = originalJumpForce;
            }
        }
    }
}
