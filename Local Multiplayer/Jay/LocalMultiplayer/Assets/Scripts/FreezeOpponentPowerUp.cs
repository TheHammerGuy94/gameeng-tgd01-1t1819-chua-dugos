﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeOpponentPowerUp : MonoBehaviour
{
    public Player otherPlayer;
	 
    // Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnTriggerEnter2D(Collider2D collidedWith)
    {
        otherPlayer.GetComponent<PlatformerMovement>().enabled = false;

        float timeRemaining = 2.0f;

        timeRemaining = timeRemaining - Time.deltaTime;

        if(timeRemaining <= 0)
        {
            otherPlayer.GetComponent<PlatformerMovement>().enabled = true;
        }
    }
}