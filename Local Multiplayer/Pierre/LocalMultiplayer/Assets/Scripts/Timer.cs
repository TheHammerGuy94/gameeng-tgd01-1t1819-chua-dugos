﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    public float timeAlloted;

	// Update is called once per frame
	void Update()
    {
        timeAlloted = timeAlloted - Time.deltaTime;

        int seconds = (int)(timeAlloted % 60);
        int minutes = (int)(timeAlloted / 60) % 60;

        if(seconds < 10)
        {
            string timeLeft = minutes + ":0" + seconds;

            timerText.text = timeLeft;
        }

        else
        {
            string timeLeft = minutes + ":" + seconds;

            timerText.text = timeLeft;
        }
    }
}