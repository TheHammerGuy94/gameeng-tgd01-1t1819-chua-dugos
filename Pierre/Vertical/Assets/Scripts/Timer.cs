﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour {
    public float time;
    public float mult = 1.0f;
    public float beepFreq = 1.0f;
    private float timeFreq = 0.0f;
    public AudioClip beep;
    private AudioSource src;

	// Use this for initialization
	void Start () {
        src = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        time -= Time.deltaTime * mult;
        timeFreq += Time.deltaTime * mult;

        if (timeFreq >= beepFreq) {
            timeFreq %= beepFreq;
            src.PlayOneShot(beep);

        }

        if (time <= 0) {
            time = 0.0f;
            print("Time's up");
            PlayerManager.instance.EndGame();
        }

	}
}
