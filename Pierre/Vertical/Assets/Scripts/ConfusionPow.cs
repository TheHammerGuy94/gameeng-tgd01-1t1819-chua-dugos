﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfusionPow : Powerup
{
    private Player px;
    public override void StartEffect()
    {
        px = PlayerManager.instance.GetOtherPlayer(this.pl);
        px.GetComponent<PlatformerMovement>().walkForce *= -1;
        px.negText.StartUpdate(this, "Confusion");
    }
    public override void EndEffect(){
        px.GetComponent<PlatformerMovement>().walkForce *= -1;
    }

}
