﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopControlsPow : Powerup
{
    private Player px = null;
    public override void StartEffect()
    {
        px = PlayerManager.instance.GetOtherPlayer(this.pl);
        px.gameObject.GetComponent<PlatformerMovement>().enabled = false;
        px.negText.StartUpdate(this, "Controls Disabled (Lols)");
    }

    public override void EndEffect()
    {
        px.gameObject.GetComponent<PlatformerMovement>().enabled = true;
    }

}
