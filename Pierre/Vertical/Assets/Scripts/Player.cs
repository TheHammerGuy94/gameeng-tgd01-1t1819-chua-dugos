﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {
    public VerticalCameraSmooth c;
    public Powerup pr = null;
    public float floorVal = 0.0f, distance = 0.0f;
    public PowerGUp posText, negText;

	// Use this for initialization
	void Start () {
        posText.gameObject.SetActive(false);
        posText.enabled = false;
        negText.gameObject.SetActive(false);
        posText.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (floorVal != distance) {
            distance = transform.position.y - floorVal;
        }
	}

    public void AddPowerup(Powerup pr) {
        this.pr = pr;
        pr.pl = this;
        pr.Activate();
        pr.StartEffect();
    }

}
