﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceGUp : PlayerUpdate
{
    private float prevDist;
    public override void ExtraStart()
    {
        prevDist = p.distance;
    }
    public override bool CheckPrevValue()
    {
        return prevDist.CompareTo(p.distance) != 0;
    }

    public override void UpdateValue()
    {
        prevDist = p.distance;
    }

    public override string GetValue()
    {
        return prevDist.ToString("G3")+"M";
    }

}
