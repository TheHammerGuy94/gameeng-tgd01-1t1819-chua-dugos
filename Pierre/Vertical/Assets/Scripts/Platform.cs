﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
    public static float minWidth = 2.5f, maxWidth = 10f;
    public static float minX = -7f, maxX = 7f;
    public PlatformSpawn spawner;
    public Powerup pr = null;
    public bool done = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (pr != null) {
            if (PlayerManager.instance.GetTrailing().transform.position.y > transform.position.y) {
                PowerupManager.instance.Recycle(pr);
                pr = null;
                this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            }
        }
	}

    public void SpawnSettings(float widthSeed, float xSeed, float y, float interval, bool jumpThrough) {
        transform.localScale = new Vector3(RandWidth(widthSeed), transform.localScale.y, transform.localScale.z);

        transform.position = new Vector2(Random.Range(minX + transform.localScale.x/2, maxX - transform.localScale.x/2), y+interval);
    }

    private float RandWidth(float widthSeed) {
        return (maxWidth - minWidth) * widthSeed + minWidth;
    }

    public void AddPowerUp(Powerup p) {
        print("adding powerup");
        if (p != null)
        {
            this.pr = p;
            p.pt = this;
            this.gameObject.GetComponent<SpriteRenderer>().color = pr.colorPlat;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player p = collision.collider.GetComponent<Player>();
        if (p != null){
            spawner.SpawnMore(this);
            if (pr != null && p.pr == null)
            {
                print("adding powerup to:" + p.gameObject.name);
                p.AddPowerup(pr);
                this.pr = null;
                this.gameObject.GetComponent<SpriteRenderer>().color = Color.white;
            }
            else {
                spawner.PowerUpPlat(this, p);
            }
        }
    }

}
