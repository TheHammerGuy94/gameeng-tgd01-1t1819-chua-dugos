﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalCameraSmooth : MonoBehaviour {
    public Rigidbody2D rb;
    public Camera c;
    public Vector3 offset = new Vector3(0, 0, -5f);
    public float multiplier = 1.0f;
    public float smoothTime = 1.0f;
    private float temp = 0f;

	// Use this for initialization
	void Start () {
        c = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        float y = Mathf.SmoothDamp(c.transform.position.y, rb.position.y + rb.velocity.y * multiplier,ref temp, smoothTime);
        c.gameObject.transform.position = new Vector3(0, y, 0) + offset;
	}
}
