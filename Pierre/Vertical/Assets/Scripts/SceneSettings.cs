﻿using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;


public class SceneSettings
{
    private static SceneSettings instance = null;

    private Dictionary<string, string> settings;

    private SceneSettings()
    {
        settings = new Dictionary<string, string>();
    }

    public static SceneSettings GetInstance(){
        if (instance == null)
            instance = new SceneSettings();
        return instance;
    }

    public void SetSetting(string setting, string value){
        if (settings.ContainsKey(setting))
            settings.Remove(setting);
        settings.Add(setting, value);

        
    }

    public string GetSetting(string setting){
        return settings.ContainsKey(setting) ? settings[setting] : "";
    }
}
