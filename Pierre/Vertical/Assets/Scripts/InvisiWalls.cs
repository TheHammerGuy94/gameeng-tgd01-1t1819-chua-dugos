﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisiWalls : MonoBehaviour {
    public Player target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.GetComponent<Player>() != null) {
            if (target == collision.GetComponent<Player>()) {
                collision.GetComponent<Rigidbody2D>().velocity = Vector2.Reflect(collision.GetComponent<Rigidbody2D>().velocity, Vector2.left);
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
        {
            if (target == collision.GetComponent<Player>())
            {
                GameObject.Find("PlatformSpawn").GetComponent<PlatformSpawn>().NearestPlatform(target);
            }
        }
    }
}
