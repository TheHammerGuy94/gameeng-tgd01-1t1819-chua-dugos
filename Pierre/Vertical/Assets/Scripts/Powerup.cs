﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Powerup : MonoBehaviour {
    public Platform pt;
    public Player pl = null;
    public float maxTime = 0, curTime = 0;
    public int maxUse = 0, curUse = 0;
    public bool neg = false;
    public Color colorPlat = Color.white;

	// Use this for initialization
	void Start () {
        this.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if(curTime > 0)
            curTime -= Time.deltaTime;
        if(curTime <= 0)
            curTime = 0;

        if ((curTime <= 0 && curUse <= 0) || pl == null) {
            //return this to powerup Spawner
            EndEffect();
            Deactivate();
            this.enabled = false;
        }
	}

    public void Activate() {
        this.enabled = true;
        print(gameObject.name + " Activated");
        print(gameObject.name + "is Active:" + this.isActiveAndEnabled);
        curTime = maxTime;
        curUse = maxUse;
    }
    public void Deactivate() {
        PowerupManager.instance.Recycle(this);
    }

    public abstract void StartEffect();
    public abstract void EndEffect();

    public void ResetUsage() {
        curTime = maxTime;
    }

    
}
