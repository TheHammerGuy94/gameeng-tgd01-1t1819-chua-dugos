﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {
    public static PowerupManager instance = null;
    public List<Powerup> availablePowerUps = new List<Powerup>();
    public List<Powerup> inUse= new List<Powerup>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);
    }

    void Start () {
        Powerup pr;
        for (int i = 0; i < transform.childCount; i++) {
            pr = transform.GetChild(i).GetComponent<Powerup>();
            availablePowerUps.Add(transform.GetChild(i).GetComponent<Powerup>());
            availablePowerUps[0].enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {}

    public void ActivatePowerup(Powerup pr) {
        pr.gameObject.SetActive(true);
    }
    
    public Powerup GeneratePowerup() {

        if (availablePowerUps.Count <= 0) {
            print("no powerups available");
            return null;
        }
        else {
            Powerup p = availablePowerUps[Random.Range(0, availablePowerUps.Count)];
            print("powerup Found");
            inUse.Add(p);
            availablePowerUps.Remove(p);
            return p;
        }
    }

    public void Recycle(Powerup pr) {
        if (pr.pl != null)
            pr.pl.pr = null;
        pr.pl = null;
        inUse.Remove(pr);
        availablePowerUps.Add(pr);
    }


}
