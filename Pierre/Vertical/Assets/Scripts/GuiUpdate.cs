﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class GuiUpdate : MonoBehaviour
{
    protected Text txt;
    protected string format;
    // Use this for initialization
    void Start()
    {
        ExtraStart();
        if (txt == null)
            txt = GetComponent<Text>();
        format = txt.text;
        UpdateText();
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckPrevValue()){
            UpdateValue();
            UpdateText();
        }
    }

    public abstract void ExtraStart();
    public abstract bool CheckPrevValue();
    public abstract void UpdateValue();

    public void UpdateText(){
        txt.text = string.Format(format, GetValue());
    }
    public abstract string GetValue();
}
