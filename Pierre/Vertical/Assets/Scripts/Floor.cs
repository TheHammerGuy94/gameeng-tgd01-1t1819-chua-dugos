﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player p = collision.collider.GetComponent<Player>();
        if ( p!= null)
        {
            if(p.floorVal == 0.0f && p.floorVal == p.distance)
                collision.collider.GetComponent<Player>().floorVal = p.gameObject.transform.position.y;
        }
    }
}
