﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerMovement : MonoBehaviour {
    /* Required Unity Behaviors:
     * Rigidbody2D, and Collider2D
     * 
     * Interacts with: Floor
     */
    private Rigidbody2D rb;
    private Collider2D cd;

    public int playerNum = 1;
    public float maxSpeed = 1.0f;
    public float walkForce = 100f;
    public float jumpForce = 500f;
    public bool isAirborne = false;
    //allows secondary jumps
    public float secondaryJumpFactor = 0.5f;
    public int jumpMax = 2, jumpCount = 0;
    private bool unlockSJump = false;
    public AudioClip jump;
    private AudioSource src;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody2D>();
        cd = GetComponent<Collider2D>();
        src = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {

        if (Mathf.Abs(Input.GetAxis("move"+ playerNum.ToString())) > 0)
        {
            Walk(Input.GetAxis("move" + playerNum.ToString()));
        }

        if (Input.anyKeyDown) {
            if (Input.GetAxis("jump"+playerNum.ToString()) > 0) {
                Jump();
                src.PlayOneShot(jump);
            }
        }

        if (Input.GetAxis("move" + playerNum.ToString()) == 0.0f) {
            Stop();
        }

    }

    public void Walk(float input) {
        rb.AddForce(Vector2.right * input * walkForce);
        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        {
            rb.velocity = new Vector2(maxSpeed * Mathf.Sign(rb.velocity.x), rb.velocity.y);
        }
    }

    public void Stop() {
        rb.velocity = new Vector2(rb.velocity.x - Mathf.Sign(rb.velocity.x)*(maxSpeed*5*Time.deltaTime), rb.velocity.y);
    }

    //adds positive vertical force onto Object
    public void Jump() {
        if (jumpCount < jumpMax) {
            rb.AddForce(Vector2.up* jumpForce * Mathf.Pow(secondaryJumpFactor,(float)jumpCount));
            jumpCount++;
        }
    }

    //resets jump
    public void Land() {
        isAirborne = false;
        if (rb.velocity.y <= 0.25f)
            jumpCount = 0;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<PlatformEffector2D>())
            Land();
    }



}
