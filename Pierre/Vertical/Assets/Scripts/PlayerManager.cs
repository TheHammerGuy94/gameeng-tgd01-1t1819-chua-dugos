﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {
    public static PlayerManager instance = null;
    public List<Player> playerList = new List<Player>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(this.gameObject);
    }

    // Use this for initialization
    void Start () {
        playerList.AddRange(FindObjectsOfType<Player>());
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    public Player GetRandomPlayer() {
        return playerList[Random.Range(0, playerList.Count)];
    }

    public Player GetOtherPlayer(Player p) {
        Player px = p;
        if (playerList.Count < 2)
            px = null;
        else
            while (px == p)
                px = playerList[Random.Range(0, playerList.Count)];
        return px;
    }

    public List<Player> GetAllOtherPlayers(Player p) {
        List<Player> newList = new List<Player>();
        newList.AddRange(playerList);
        newList.Remove(p);
        return newList;
    }

    public Player GetLeading() {
        Player pMax = playerList[0];
        foreach (Player p in playerList)
            if (p.distance > pMax.distance)
                pMax = p;
        return pMax;
    }

    public Player GetTrailing() {
        Player pMin = playerList[0];
        foreach (Player p in playerList)
            if (p.distance < pMin.distance)
                pMin = p;
        return pMin;
    }
    public bool isLeading(Player p) {
        return GetLeading() == p;
    }
    public bool isTrailing(Player p) {
        return GetTrailing() == p;
    }
    public void EndGame() {
        SceneSettings.GetInstance().SetSetting("player1Score", playerList[0].distance.ToString("G3"));
        SceneSettings.GetInstance().SetSetting("player2Score", playerList[1].distance.ToString("G3"));
        SceneSettings.GetInstance().SetSetting("winner", GetLeading().gameObject.name);
        print("GameDone!!!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
}
