﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpPow : Powerup
{
    public float factor = 2;

    public override void StartEffect()
    {
        pl.gameObject.GetComponent<PlatformerMovement>().jumpForce *= Mathf.Sqrt(factor);
        pl.posText.StartUpdate(this, "Double Jumping Height");
    }
    public override void EndEffect()
    {
        pl.gameObject.GetComponent<PlatformerMovement>().jumpForce /= Mathf.Sqrt(factor);
    }
}
