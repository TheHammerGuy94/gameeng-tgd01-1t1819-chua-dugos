﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawn : MonoBehaviour {
    public Platform plat;
    public List<Platform> spawnList = new List<Platform>();
    public float interval = 5;
    public int ahead = 5;
    public float leadingChance = 0.2f, trailingChance = 0.8f, otherChance = 0.5f;

	// Use this for initialization
	void Start () {
        foreach (Platform platform in spawnList)
        {
            platform.spawner = this;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Spawn() {
        Platform px = Instantiate(plat);
        px.SpawnSettings(Random.value, Random.value, spawnList[spawnList.Count-1].transform.position.y,interval, true);
        px.spawner = this;
        spawnList.Add(px);
    }

    public void SpawnMore(Platform p) {
        int target = spawnList.IndexOf(p) + ahead;
        while (spawnList.Count < target) Spawn();
    }

    public void PowerUpPlat(Platform p,Player pl) {
        if (pl.pr == null)
            if (PlayerManager.instance.isTrailing(pl))
            {
                if (Random.value < trailingChance)
                {
                    SpawnPowerup(p);
                }
            }
            else if (PlayerManager.instance.isLeading(pl))
            {
                if (Random.value < leadingChance)
                {
                    SpawnPowerup(p);
                }
            }
            else {
                if (Random.value < otherChance)
                {
                    SpawnPowerup(p);
                }
            }

    }

    private void SpawnPowerup(Platform p) {
        int i = spawnList.IndexOf(p);
        if(spawnList[i + 1].pr == null)
            spawnList[i + 1].AddPowerUp(PowerupManager.instance.GeneratePowerup());
    }
    public void NearestPlatform(Player p) {
        int i = Mathf.FloorToInt(p.transform.position.y / interval) - 1;
        Platform pNear = spawnList[Mathf.Max(0,i)];

        p.transform.position = pNear.transform.position + new Vector3(0, 1);

    }
}
