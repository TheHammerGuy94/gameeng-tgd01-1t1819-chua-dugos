﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HijackPow : Powerup
{
    private Player px;
    private int playNumTarg, playNumSelf;
    public override void StartEffect()
    {
        px = PlayerManager.instance.GetOtherPlayer(this.pl);
        //store the player nums
        playNumSelf = pl.gameObject.GetComponent<PlatformerMovement>().playerNum;
        playNumTarg = px.gameObject.GetComponent<PlatformerMovement>().playerNum;

        //swap player takes opponent's controls (and camera)
        px.gameObject.GetComponent<PlatformerMovement>().playerNum = playNumSelf;
        pl.gameObject.GetComponent<PlatformerMovement>().playerNum = -1;
        pl.c.rb = px.gameObject.GetComponent<Rigidbody2D>();

        //game places player to the nearest platform
        pl.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        pl.transform.position = pt.transform.position + new Vector3(0, 1, 0);



        px.negText.StartUpdate(this, "Controls Hijacked");
        pl.posText.StartUpdate(this, "You Hijacked Opponent");

    }
    public override void EndEffect()
    {
        px.gameObject.GetComponent<PlatformerMovement>().playerNum = playNumTarg;
        px.c.rb = px.gameObject.GetComponent<Rigidbody2D>();

        pl.gameObject.GetComponent<PlatformerMovement>().playerNum = playNumSelf;
        pl.c.rb = pl.gameObject.GetComponent<Rigidbody2D>();
    }

}
