﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerGUp : MonoBehaviour
{
    private Text t;
    private Powerup p;
    private string format;

    private void Start()
    {
        t = GetComponent<Text>();
    }

    private void Update()
    {
        t.text = string.Format(format, p.curTime.ToString("G2"));

        if (!p.enabled) {
            this.gameObject.SetActive(false);
            this.enabled = false;
        }
    }



    public void StartUpdate(Powerup p) {
        StartUpdate(p, p.gameObject.name);
    }
    public void StartUpdate(Powerup p, string name) {
        this.p = p;
        this.format = name + ": {0}s";
        this.gameObject.SetActive(true);
        this.enabled = true;
    }
}
