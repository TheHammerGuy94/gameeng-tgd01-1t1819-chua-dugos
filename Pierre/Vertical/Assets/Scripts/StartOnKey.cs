﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class StartOnKey : MonoBehaviour
{
    public int sceneNumberToLoad;
    
    // Use this for initialization
	void Start ()
    {
	    
	}
    
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Space))
        {
            LoadScene();
        }

        if(Input.GetKeyDown(KeyCode.Escape) && sceneNumberToLoad <= 2)
        {
            Application.Quit();
        }        
	}

    private void LoadScene()
    {
        SceneManager.LoadScene(sceneNumberToLoad);
    }
}
