﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerGUp : GuiUpdate
{
    public Timer t;
    private float prevTime;

    public override void ExtraStart()
    {
        prevTime = t.time;
    }

    public override bool CheckPrevValue()
    {
        return t.time.CompareTo(prevTime) != 0;
    }

    public override string GetValue()
    {
        return prevTime.ToString("G2");
    }

    public override void UpdateValue()
    {
        prevTime = t.time;
    }
}
