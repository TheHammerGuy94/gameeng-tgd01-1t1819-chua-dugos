﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeFreezePow : Powerup
{
    
    public override void StartEffect()
    {
        print("Slowing time by: "+pl.gameObject.name);
        FindTimer().mult = 0f;
        pl.posText.StartUpdate(this, "Time Frozen");
    }

    public override void EndEffect()
    {
        FindTimer().mult = 1.0f;
    }
    private Timer FindTimer() {
        return GameObject.Find("Timer").GetComponent<Timer>();
    }

}
