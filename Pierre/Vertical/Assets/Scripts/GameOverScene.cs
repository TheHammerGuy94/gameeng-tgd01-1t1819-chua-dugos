﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScene : MonoBehaviour {
    public Text p1Score, p2Score;
    public Text cmd;

    public float pressable = 2.0f;
    private float time = 0.0f;


	// Use this for initialization
	void Start () {
        p1Score.text = string.Format(p1Score.text, SceneSettings.GetInstance().GetSetting("player1Score"));
        p2Score.text = string.Format(p2Score.text, SceneSettings.GetInstance().GetSetting("player2Score"));
        cmd.text = string.Format(cmd.text, SceneSettings.GetInstance().GetSetting("winner"));

	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;

        if (Input.anyKeyDown && time > pressable) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
        }
		
	}
}
